<?php

class Talentos extends SessionController
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserSessionData();

        error_log('TALENTOS::construct -> Inicio de Empresas');
    }

    function render()
    {
        error_log('TALENTOS::render -> Carga Index de Empresas');
        $talentosInteresados = [];

        $usuarioModel = new UserModel();
        $usuariosTotales = count($usuarioModel->getAll());

        $empresaModel = new EmpresaModel();
        $empresas = $empresaModel->getAll();
        $empresasTotales = count($empresaModel->getAll());

        $talentoModel = new TalentoModel();
        $talentos = $talentoModel->getAll();
        $talentosTotales = count($talentoModel->getAll());

        $ofertaModel = new OfertaModel();
        $ofertas = $ofertaModel->getAll();
        $ofertasTotales = count($ofertas);

        $empresaId = $empresaModel->get($this->user->getId())->getEmpresaId();

        $empresaTalentoModel = new EmpresaTalentoModel();
        $empresaTalento = $empresaTalentoModel->getByEmpresa($this->user->getId());
        $empresaTalentoByEmpresa = $empresaTalentoModel->getByEmpresa($empresaId);

        foreach ($empresaTalentoByEmpresa as $key => $value) {
            $item = new TalentoModel();
            $talentoId = $value->getTalentoId();
            $talento = $item->getByTalentoId($talentoId);
            array_push($talentosInteresados, $talento);
        }

        $this->view->render('talentos/index', [
            'user'                  => $this->user,
            'ofertas'               => $ofertas,
            'talentos'              => $talentos,
            'empresas'              => $empresas,
            'usuariosTotales'       => $usuariosTotales,
            'empresasTotales'       => $empresasTotales,
            'talentosTotales'       => $talentosTotales,
            'ofertasTotales'        => $ofertasTotales,
            'empresaTalento'        => $empresaTalento,
            'talentosInteresados'   => $talentosInteresados
        ]);
        //$this->view->render('empresas/index');
    }
    function interes()
    {
        $empresaModel = new EmpresaModel();
        $empresaId = $empresaModel->get($this->user->getId())->getEmpresaId();
        $talentoId = $_POST['talentoId'];
        $empresaTalentoModel = new EmpresaTalentoModel();
        if (!$empresaTalentoModel->exists($talentoId, $empresaId)) {
            $empresaTalentoModel->setTalentoId($talentoId);
            $empresaTalentoModel->setEmpresaId($empresaId);
            $empresaTalentoModel->save();
        }
    }
}
