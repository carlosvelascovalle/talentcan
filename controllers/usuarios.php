<?php

class User extends SessionController
{
    private $user;

    function __construct()
    {
        parent::__construct();

        $this->user = $this->getUserSessionData();
        error_log("USER::__construct -> " . $this->user->getUsername());
    }

    function render()
    {
        $this->view->render('user/index', [
            "user" => $this->user
        ]);
    }
}
