<?php

class InteresMutuo extends SessionController
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserSessionData();

        error_log('INTERESMUTUO::construct -> Inicio de Interes Mutuo');
    }

    function render()
    {
        error_log('INTERESMUTUO::render -> Carga Index de Interes Mutuo');

        $empresaTalentos = [];
        $talentoEmpresas = [];
        $empresasMutuas = [];
        $talentosMutuos = [];

        $usuarioModel = new UserModel();
        $usuariosTotales = count($usuarioModel->getAll());

        $empresaModel = new EmpresaModel();
        $empresasTotales = count($empresaModel->getAll());

        $talentoModel = new TalentoModel();
        $talentos = $talentoModel->getAll();
        $talentosTotales = count($talentoModel->getAll());

        $ofertaModel = new OfertaModel();
        $ofertas = $ofertaModel->getAll();
        $ofertasTotales = count($ofertas);
        $userRole = $this->user->getRole();

        $empresaTalentoModel = new EmpresaTalentoModel();
        $talentoEmpresaModel = new TalentoEmpresaModel();
        if ($userRole == 'talento') {
            $talentoId = $talentoModel->get($this->user->getId())->getTalentoId();
            $talentoEmpresa = $talentoEmpresaModel->getByTalento($talentoId);
            $empresaTalento = $empresaTalentoModel->getByTalento($talentoId);
        }
        if ($userRole == 'empresa') {
            $empresaId = $empresaModel->get($this->user->getId())->getEmpresaId();
            $talentoEmpresa = $talentoEmpresaModel->getByEmpresa($empresaId);
            $empresaTalento = $empresaTalentoModel->getByEmpresa($empresaId);
        }
        foreach ($empresaTalento as $key => $value) {
            
            if ($userRole == 'talento') {
                $itemEmpresa = new EmpresaModel();
                $empresaId = $value->getEmpresaId();
                $empresa = $itemEmpresa->getByEmpresaId($empresaId);
                array_push($empresaTalentos, $empresa);
            }
            if ($userRole == 'empresa') {
                $itemTalento = new TalentoModel();
                $talentoId = $value->getTalentoId();
                $talento = $itemTalento->getByTalentoId($talentoId);
                array_push($empresaTalentos, $talento);
            }
        }

        foreach ($talentoEmpresa as $key => $value) {
            if ($userRole == 'talento') {
                $itemEmpresa = new EmpresaModel();
                $empresaId = $value->getEmpresaId();
                $empresa = $itemEmpresa->getByEmpresaId($empresaId);
                array_push($talentoEmpresas, $empresa);
            }
            if ($userRole == 'empresa') {
                $itemTalento = new TalentoModel();
                $talentoId = $value->getTalentoId();
                $talento = $itemTalento->getByTalentoId($talentoId);
                array_push($talentoEmpresas, $talento);
            }
        }

        foreach ($talentoEmpresas as $item1) {
            foreach ($empresaTalentos as $item2) {
                if ($item1 == $item2) {
                    if ($empresaModel->exists($item1->getNombre())) {
                        array_push($empresasMutuas, $item1);
                    }
                    if ($talentoModel->exists($item1->getNombre())) {
                        array_push($talentosMutuos, $item1);
                        
                    }
                }
            }
        }


        $this->view->render('interesMutuo/index', [
            'user'                  => $this->user,
            'ofertas'               => $ofertas,
            'empresasMutuas'        => $empresasMutuas,
            'talentosMutuos'        => $talentosMutuos,
            'talentos'              => $talentos,
            'usuariosTotales'       => $usuariosTotales,
            'empresasTotales'       => $empresasTotales,
            'talentosTotales'       => $talentosTotales,
            'ofertasTotales'        => $ofertasTotales,
            'empresaTalentos'       => $empresaTalentos,
            'talentoEmpresas'       => $talentoEmpresas
        ]);
    }
    function interes()
    {
        $talentoModel = new TalentoModel();
        $talentoId = $talentoModel->get($this->user->getId())->getTalentoId();
        $empresaId = $_POST['empresaId'];
        $empresaTalentoModel = new EmpresaTalentoModel();

        if ($empresaTalentoModel->exists($talentoId, $empresaId)) {
            $empresa_talento_id = $empresaTalentoModel->getByEmpresaTalento(($talentoId), intval($empresaId));
            $empresaTalentoModel->delete($empresa_talento_id->getId());
        }
    }
}
