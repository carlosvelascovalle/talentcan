<?php

class Dashboard extends SessionController{

    function __construct(){
        parent::__construct();
        $this->user = $this->getUserSessionData();

        error_log('DASHBOARD::construct -> Inicio de Dashboard');
    }

    function render(){
        error_log('DASHBOARD::render -> Carga Index de Dashboard');
        $usuarioModel = new UserModel();
        $usuarios =$usuarioModel->getAll();
        $usuariosTotales = count($usuarios);

        $empresaModel = new EmpresaModel();
        $empresas =$empresaModel->getAll();
        $empresasTotales = count($empresas);

        $talentoModel = new TalentoModel();
        $talentos = $talentoModel->getAll();
        $talentosTotales = count($talentos);

        $ofertaModel = new OfertaModel();
        $ofertas = $ofertaModel->getAllOrderBySalario();
        $ofertasTotales = count($ofertas);
        //$ofertaByAptitud = $ofertaModel->getByAptitudId(1);



        $this->view->render('dashboard/index', [
            'user'                  => $this->user,
            'ofertas'               => $ofertas,
            //'ofertaByAptitud'       => $ofertaByAptitud,
            'talentos'              => $talentos,
            'empresas'              => $empresas,
            'usuariosTotales'       => $usuariosTotales,
            'empresasTotales'       => $empresasTotales,
            'talentosTotales'       => $talentosTotales,
            'ofertasTotales'        => $ofertasTotales,
        ]);
        //$this->view->render('dashboard/index');
    }

}