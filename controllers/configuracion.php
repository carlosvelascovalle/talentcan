<?php

class Configuracion extends SessionController
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserSessionData();
        $this->talentoModel = new TalentoModel();
        $this->empresaModel = new EmpresaModel();
        $this->aptitudModel = new AptitudModel();
        $this->aptitudTalentoModel = new AptitudTalentoModel();
        $this->talento = $this->talentoModel->get($this->user->getId());
        $this->empresa = $this->empresaModel->get($this->user->getId());
        $this->aptitudesAll = $this->aptitudModel->getAll();



        error_log('CONFIGURACIÓN::construct -> Inicio de Configuración');
    }

    function render()
    {
        $usuarioModel = new UserModel();
        $usuarios = $usuarioModel->getAll();
        $usuariosTotales = count($usuarios);

        $empresaModel = new EmpresaModel();
        $empresas = $empresaModel->getAll();
        $empresasTotales = count($empresas);

        $talentoModel = new TalentoModel();
        $talentos = $talentoModel->getAll();
        $talentosTotales = count($talentos);

        $ofertaModel = new OfertaModel();
        $ofertas = $ofertaModel->getAll();
        $ofertasTotales = count($ofertas);

        $aptitudes = [];
        $aptitudTalento = [];
        $aptitudesTodas = [];

        error_log('CONFIGURACIÓN::render -> Carga Index de Configuración');
        if ($this->talento->getTalentoId()) {
            foreach ($this->aptitudesAll as $aptitud) {
                array_push($aptitudesTodas, [$aptitud->getId() => $aptitud->getNombre()]);
            }

            $aptitudTalento = $this->aptitudTalentoModel->getByTalento($this->talento->getTalentoId());
            foreach ($aptitudTalento as $aptitud) {
                array_push($aptitudes, [$this->aptitudModel->get($aptitud->getAptitudId())->getId() => $this->aptitudModel->get($aptitud->getAptitudId())->getNombre()]);
            }
        }
        if ($this->empresa->getEmpresaId()) {
        }


        $this->view->render('configuracion/index', [
            'user'                  => $this->user,
            'talento'               => $this->talento,
            'empresa'               => $this->empresa,
            'aptitudes'             => $aptitudes,
            'aptitudTalento'        => $aptitudTalento,
            'aptitudesTodas'        => $aptitudesTodas,
            'usuariosTotales'       => $usuariosTotales,
            'empresasTotales'       => $empresasTotales,
            'talentosTotales'       => $talentosTotales,
            'ofertasTotales'        => $ofertasTotales,
        ]);
    }

    function guardar()
    {
        if ($this->existPOST(['nombre', 'email'])) {
            error_log('CONFIGURACION::guardar() => ExistPOST()');
            $talentoModel = new TalentoModel();
            $talento = $talentoModel->get($this->user->getId());

            $empresaModel = new EmpresaModel();
            $empresa = $empresaModel->get($this->user->getId());

            $nombre = $this->getPOST('nombre');
            $email = $this->getPOST('email');
            $habilidades = $this->getPOST('habilidades');
            $apellidos = '';
            $sector = '';
            $localizacion = '';
            $web = '';
            $estudios_nombre = '';
            $estudios_tipo = '';
            $estudios_media = '';
            if ($this->existPOST(['apellidos'])) {
                $apellidos = $this->getPOST('apellidos');
            }
            if ($this->existPOST(['sector'])) {
                $sector = $this->getPOST('sector');
            }
            if ($this->existPOST(['localizacion'])) {
                $localizacion = $this->getPOST('localizacion');
            }
            if ($this->existPOST(['web'])) {
                $web = $this->getPOST('web');
            }
            if ($this->existPOST(['estudios'])) {
                $estudios_nombre = $this->getPOST('estudios');
            }
            if ($this->existPOST(['cualificacion'])) {
                $estudios_tipo = $this->getPOST('cualificacion');
            }
            if ($this->existPOST(['media'])) {
                $estudios_media = $this->getPOST('media');
                error_log($estudios_media);
            }

            if ($talento->getTalentoId()) {
                $usuario = new TalentoModel();
                $usuario->setNombre($nombre);
                $usuario->setApellidos($apellidos);
                $usuario->setEmail($email);
                $usuario->setEstudiosNombre($estudios_nombre);
                $usuario->setEstudiosTipo($estudios_tipo);
                $usuario->setEstudiosMedia($estudios_media);
                $usuario->setUserId($this->user->getId());

                $usuario->update();
            }
            if ($this->user->getRole() == 'empresa') {
                error_log($this->user->getId());
                $usuario = new EmpresaModel();
                $usuario->setNombre($nombre);
                $usuario->setApellidos($apellidos);
                $usuario->setEmail($email);
                $usuario->setSector($sector);
                $usuario->setLocalizacion($localizacion);
                $usuario->setWeb($web);
                $usuario->setUserId($this->user->getId());

                $usuario->update();
            }
            $this->redirect('configuracion');
        }
    }
}
