<?php

class Ofertas extends SessionController
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserSessionData();

        error_log('OFERTAS::construct -> Inicio de Empresas');
    }

    function render()
    {
        error_log('OFERTAS::render -> Carga Index de Empresas');

        $empresasInteresadas = [];

        $usuarioModel = new UserModel();
        $usuariosTotales = count($usuarioModel->getAll());

        $empresaModel = new EmpresaModel();
        $empresas = $empresaModel->getAll();
        $empresasTotales = count($empresaModel->getAll());

        $talentoModel = new TalentoModel();
        $talentos = $talentoModel->getAll();
        $talentosTotales = count($talentoModel->getAll());

        $ofertaModel = new OfertaModel();
        $ofertas = $ofertaModel->getAll();
        $getAllBySalario = $ofertaModel->getAllOrderBySalario();
        $ofertasTotales = count($ofertas);

        $talentoEmpresaModel = new TalentoEmpresaModel();
        $talentoEmpresa = $talentoEmpresaModel->getByTalento($this->user->getId());

        $talentoId = $talentoModel->get($this->user->getId())->getTalentoId();
        $talentoEmpresaByTalento = $talentoEmpresaModel->getByTalento($talentoId);

        foreach ($talentoEmpresaByTalento as $key => $value) {
            $item = new EmpresaModel();
            $empresaId = $value->getEmpresaId();
            $empresa = $item->getByEmpresaId($empresaId);
            array_push($empresasInteresadas, $empresa);
        }

        $this->view->render('ofertas/index', [
            'user'                  => $this->user,
            'ofertas'               => $ofertas,
            'talentos'              => $talentos,
            'empresas'              => $empresas,
            'usuariosTotales'       => $usuariosTotales,
            'empresasTotales'       => $empresasTotales,
            'talentosTotales'       => $talentosTotales,
            'ofertasTotales'        => $ofertasTotales,
            'talentoEmpresa'        => $talentoEmpresa,
            'empresasInteresadas'   => $empresasInteresadas,
            'ofertaModel'           => $ofertaModel,
            'getAllBySalario'       => $getAllBySalario
        ]);
    }
    function interes()
    {
        $talentoModel = new TalentoModel();
        $talentoId = $talentoModel->get($this->user->getId())->getTalentoId();
        $empresaId = $_POST['empresaId'];
        $talentoEmpresaModel = new TalentoEmpresaModel();
        error_log($talentoId . '-->' . $empresaId);
        if (!$talentoEmpresaModel->exists($talentoId, $empresaId)) {
            $talentoEmpresaModel->setTalentoId($talentoId);
            $talentoEmpresaModel->setEmpresaId($empresaId);
            $talentoEmpresaModel->save();
        }
    }

    function buscarOferta()
    {
        
    }
}
