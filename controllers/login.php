<?php
require_once 'libs/model.php';

class Login extends SessionController{

    function __construct(){
        parent::__construct();
        error_log('Login::construct -> Inicio de Login');

    }

    function render(){
        error_log('Login::render -> Carga Index de Login');
        $this->view->render('login/index');
    }

    function authenticate(){
        if($this->existPOST(['username','password'])){
            $username = $this->getPOST('username');
            $password = $this->getPOST('password');
            //Validar los datos
            if ($username == '' || empty($username) || $password == '' || empty($password)) {
                $this->redirect('', ['error'=>ErrorMessages::ERROR_LOGIN_AUTHENTICATE_EMPTY]);
                return;
            }
            $user = $this->model->login($username, $password);
            
            if($user != NULL){
                $this->initialize($user);
            }else{
                error_log('LOGIN::authenticate -> username and/or password wrong');
                $this->redirect('', ['error' => ErrorMessages::ERROR_LOGIN_AUTHENTICATE_DATA]);
                return;
            }

        }else{
            error_log('LOGIN::authenticate -> error with params');
            $this->redirect('', ['error' => ErrorMessages::ERROR_LOGIN_AUTHENTICATE]);
        } 
    }

}
