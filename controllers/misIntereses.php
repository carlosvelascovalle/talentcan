<?php

class MisIntereses extends SessionController
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserSessionData();

        error_log('MISINTERESES::construct -> Inicio de Mis Intereses');
    }

    function render()
    {
        error_log('MISINTERESES::render -> Carga Index de Mis Intereses');
        $usuarioModel = new UserModel();
        $usuariosTotales = count($usuarioModel->getAll());

        $empresaModel = new EmpresaModel();
        $empresas = [];
        $empresasTotales = count($empresaModel->getAll());

        $talentoModel = new TalentoModel();
        $talentos = [];
        $talentosTotales = count($talentoModel->getAll());

        $ofertaModel = new OfertaModel();
        $ofertas = $ofertaModel->getAll();
        $ofertasTotales = count($ofertas);

        $talentoEmpresaModel = new TalentoEmpresaModel();
        $talentoId = $talentoModel->get($this->user->getId())->getTalentoId();
        $talentoEmpresa = $talentoEmpresaModel->getByTalento($talentoId);

        $empresaTalentoModel = new EmpresaTalentoModel();
        $empresaId = $empresaModel->get($this->user->getId())->getEmpresaId();
        $empresaTalento = $empresaTalentoModel->getByEmpresa($empresaId);

        if ($this->user->getRole() == 'talento') {
            foreach ($talentoEmpresa as $key => $value) {
                $item = new EmpresaModel();
                $empresaId = $value->getEmpresaId();
                $empresa = $item->getByEmpresaId($empresaId);
                array_push($empresas, $empresa);
            }
        }
        if ($this->user->getRole() == 'empresa') {
            foreach ($empresaTalento as $key => $value) {
                $item = new TalentoModel();
                $talentoId = $value->getTalentoId();
                $talento = $item->getByTalentoId($talentoId);
                array_push($talentos, $talento);
            }
        }
        $this->view->render('misIntereses/index', [
            'user'                  => $this->user,
            'ofertas'               => $ofertas,
            'talentos'              => $talentos,
            'empresas'              => $empresas,
            'usuariosTotales'       => $usuariosTotales,
            'empresasTotales'       => $empresasTotales,
            'talentosTotales'       => $talentosTotales,
            'ofertasTotales'        => $ofertasTotales,
        ]);
    }
    function desinteres()
    {
        if ($this->user->getRole() == 'talento') {
            $talentoModel = new TalentoModel();
            $talentoId = $talentoModel->get($this->user->getId())->getTalentoId();
            $empresaId = $_POST['empresaId'];
            $talentoEmpresaModel = new TalentoEmpresaModel();

            if ($talentoEmpresaModel->exists($talentoId, $empresaId)) {
                $empresa_talento_id = $talentoEmpresaModel->getByTalentoEmpresa(($talentoId), intval($empresaId));
                $talentoEmpresaModel->delete($empresa_talento_id->getId());
            }
        }

        if ($this->user->getRole() == 'empresa') {
            $empresaModel = new EmpresaModel();
            $empresaId = $empresaModel->get($this->user->getId())->getEmpresaId();
            $talentoId = $_POST['talentoId'];
            $empresaTalentoModel = new EmpresaTalentoModel();

            if ($empresaTalentoModel->exists($empresaId, $empresaId)) {
                $talento_empresa_id = $empresaTalentoModel->getByEmpresaTalento(($talentoId), intval($empresaId));
                $empresaTalentoModel->delete($talento_empresa_id->getId());
            }
        }
    }
}
