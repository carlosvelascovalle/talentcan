<?php

class Signup extends SessionController
{

    function __construct()
    {
        parent::__construct();
    }

    function render()
    {
        $this->view->render('login/signup', []);
    }

    function newUser()
    {
        if ($this->existPOST(['email', 'password'])) {
            error_log('SIGNUP::newUser() => ExistPOST()');
            $nombre = $this->getPOST('nombre');
            $apellidos = $this->getPOST('apellidos');
            $username = $this->getPOST('email');
            $password = $this->getPOST('password');
            $rol = $this->getPOST('rol');

            error_log($nombre);
            error_log($apellidos);
            error_log($username);
            error_log($password);
            error_log($rol);

            if ($nombre == '' || empty($nombre) || $username == '' || empty($username) || $password == '' || empty($password)) {
                $this->redirect('signup', ['error' => ErrorMessages::ERROR_SIGNUP_NEWUSER_EMPTY]);
            } else {
                $user = new UserModel();
                $user->setUsername($username);
                $user->setPassword($password);
                $user->setRole($rol);

                if ($rol == 'talento') {
                    $talento = new TalentoModel();
                    $talento->setNombre($nombre);
                    $talento->setApellidos($apellidos);
                    $talento->setEmail($username);
                }

                if ($rol == 'empresa') {
                    $empresa = new EmpresaModel();
                    $empresa->setNombre($nombre);
                    $empresa->setApellidos($apellidos);
                    $empresa->setEmail($username);
                }

                if ($user->exists($username)) {
                    error_log('SIGNUP::newUser() => usuario ya existe');
                    $this->redirect('signup', ['error' => ErrorMessages::ERROR_SIGNUP_NEWUSER_EXISTS]);
                } else if ($user->save()) {
                    error_log('SIGNUP::newUser() => usuario guardado');
                    $userCheck = new UserModel();
                    $userCheckArray = $userCheck->getByUsername($username);
                    $userId = $userCheckArray->getId();

                    error_log('SIGNUP::newUser() => usuario guardado'. $userId);
                    if ($rol == 'talento') {
                        $talento->setUserId($userId);
                        $talento->save();
                    }

                    if ($rol == 'empresa') {
                        $empresa->setUserId($userId);
                        $empresa->save();
                    }
                    $this->redirect('', ['success' => SuccessMessages::SUCCESS_SIGNUP_NEWUSER]);
                } else {
                    error_log('SIGNUP::newUser() => error1');
                    $this->redirect('signup', ['error' => ErrorMessages::ERROR_SIGNUP_NEWUSER]);
                }
            }
        } else {
            error_log('SIGNUP::newUser() => error2');
            $this->redirect('signup', ['error' => ErrorMessages::ERROR_SIGNUP_NEWUSER]);
        }
    }
}
