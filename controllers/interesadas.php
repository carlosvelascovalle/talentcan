<?php

class Interesadas extends SessionController
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserSessionData();

        error_log('INTERESADAS::construct -> Inicio de Interesadas');
    }

    function render()
    {
        error_log('INTERESADAS::render -> Carga Index de Interesadas');

        $empresaTalentos = [];
        $talentoEmpresas = [];
        $empresasInteresadas = [];
        $talentosInteresados = [];

        $usuarioModel = new UserModel();
        $usuariosTotales = count($usuarioModel->getAll());

        $empresaModel = new EmpresaModel();
        $empresasTotales = count($empresaModel->getAll());

        $talentoModel = new TalentoModel();
        $talentos = $talentoModel->getAll();
        $talentosTotales = count($talentoModel->getAll());

        $ofertaModel = new OfertaModel();
        $ofertas = $ofertaModel->getAll();
        $ofertasTotales = count($ofertas);

        $talentoId = $talentoModel->get($this->user->getId())->getTalentoId();
        $empresaId = $empresaModel->get($this->user->getId())->getEmpresaId();

        $empresaTalentoModel = new EmpresaTalentoModel();
        $empresaTalento = $empresaTalentoModel->getByTalento($talentoId);

        $talentoEmpresaModel = new TalentoEmpresaModel();
        $talentoEmpresa = $talentoEmpresaModel->getByEmpresa($empresaId);


        foreach ($empresaTalento as $key => $value) {
            $item = new EmpresaModel();
            $empresaId = $value->getEmpresaId();
            $empresa = $item->getByEmpresaId($empresaId);
            array_push($empresaTalentos, $empresa);
        }

        foreach ($talentoEmpresa as $key => $value) {
            $item = new TalentoModel();
            $talentoId = $value->getTalentoId();
            $talento = $item->getByTalentoId($talentoId);
            array_push($talentoEmpresas, $talento);
        }

        $talentoEmpresaByTalento = $talentoEmpresaModel->getByTalento($talentoId);
        $empresaTalentoByEmpresa = $empresaTalentoModel->getByEmpresa($empresaId);

        foreach ($talentoEmpresaByTalento as $key => $value) {
            $item = new EmpresaModel();
            $empresaId = $value->getEmpresaId();
            $empresa = $item->getByEmpresaId($empresaId);
            array_push($empresasInteresadas, $empresa);
        }
        foreach ($empresaTalentoByEmpresa as $key => $value) {
            $item = new TalentoModel();
            $talentoId = $value->getTalentoId();
            $talento = $item->getByTalentoId($talentoId);
            array_push($talentosInteresados, $talento);
        }

        $this->view->render('interesadas/index', [
            'user'                  => $this->user,
            'ofertas'               => $ofertas,
            'talentos'              => $talentos,
            'usuariosTotales'       => $usuariosTotales,
            'empresasTotales'       => $empresasTotales,
            'talentosTotales'       => $talentosTotales,
            'ofertasTotales'        => $ofertasTotales,
            'empresaTalentos'       => $empresaTalentos,
            'talentoEmpresas'       => $talentoEmpresas,
            'empresasInteresadas'  => $empresasInteresadas,
            'talentosInteresados'  => $talentosInteresados
        ]);
    }
    function interes()
    {
        if ($this->user->getRole() == 'talento') {
            $talentoModel = new TalentoModel();
            $talentoId = $talentoModel->get($this->user->getId())->getTalentoId();
            $empresaId = $_POST['empresaId'];
            $empresaTalentoModel = new EmpresaTalentoModel();

            if ($empresaTalentoModel->exists($talentoId, $empresaId)) {
                $empresa_talento_id = $empresaTalentoModel->getByEmpresaTalento(($talentoId), intval($empresaId));
                $empresaTalentoModel->delete($empresa_talento_id->getId());
            }
        }

        if ($this->user->getRole() == 'empresa') {
            $empresaModel = new EmpresaModel();
            $empresaId = $empresaModel->get($this->user->getId())->getEmpresaId();
            $talentoId = $_POST['talentoId'];
            $talentoEmpresaModel = new TalentoEmpresaModel();

            if ($talentoEmpresaModel->exists($talentoId, $empresaId)) {
                $talento_empresa_id = $talentoEmpresaModel->getByTalentoEmpresa(($talentoId), intval($empresaId));
                $talentoEmpresaModel->delete($talento_empresa_id->getId());
            }
        }
    }
}
