<?php

class NuevaOferta extends SessionController
{

    function __construct()
    {
        parent::__construct();
        $this->user = $this->getUserSessionData();
        $this->userModel = new UserModel();
        $this->talentoModel = new TalentoModel();
        $this->empresaModel = new EmpresaModel();
        $this->ofertaModel = new OfertaModel();
        $this->aptitudModel = new AptitudModel();
        $this->aptitudTalentoModel = new AptitudTalentoModel();
        $this->talento = $this->talentoModel->get($this->user->getId());
        $this->empresa = $this->empresaModel->get($this->user->getId());
        $this->aptitudesAll = $this->aptitudModel->getAll();

        error_log('NUEVA OFERTA::construct -> Inicio de Nueva Oferta');
    }

    function render()
    {
        $usuarios = $this->userModel->getAll();
        $usuariosTotales = count($usuarios);

        $empresas = $this->empresaModel->getAll();
        $empresasTotales = count($empresas);

        $talentos = $this->talentoModel->getAll();
        $talentosTotales = count($talentos);

        $ofertas = $this->ofertaModel->getAll();
        $ofertasTotales = count($ofertas);

        $aptitudes = [];
        $aptitudTalento = [];
        $aptitudesTodas = [];

        error_log('NUEVA OFERTA::render -> Carga Index de Nueva Oferta');
        if ($this->talento->getTalentoId()) {
            foreach ($this->aptitudesAll as $aptitud) {
                array_push($aptitudesTodas, [$aptitud->getId() => $aptitud->getNombre()]);
            }

            $aptitudTalento = $this->aptitudTalentoModel->getByTalento($this->talento->getTalentoId());
            foreach ($aptitudTalento as $aptitud) {
                array_push($aptitudes, [$this->aptitudModel->get($aptitud->getAptitudId())->getId() => $this->aptitudModel->get($aptitud->getAptitudId())->getNombre()]);
            }
        }
        if ($this->empresa->getEmpresaId()) {
        }


        $this->view->render('nuevaOferta/index', [
            'user'                  => $this->user,
            'talento'               => $this->talento,
            'empresa'               => $this->empresa,
            'aptitudes'             => $aptitudes,
            'aptitudTalento'        => $aptitudTalento,
            'aptitudesTodas'        => $aptitudesTodas,
            'usuariosTotales'       => $usuariosTotales,
            'empresasTotales'       => $empresasTotales,
            'talentosTotales'       => $talentosTotales,
            'ofertasTotales'        => $ofertasTotales,
        ]);
    }

    function crear()
    {
        if ($this->existPOST(['nombre', 'salario', 'descripcion'])) {
            error_log('NUEVA OFERTA::crear() => ExistPOST()');

            $empresaModel = new EmpresaModel();
            $empresa = $empresaModel->get($this->user->getId());
            $empresaId = $empresa->getempresaId();

            if ($this->existPOST(['nombre'])) {
                $nombre = $this->getPOST('nombre');
            }
            if ($this->existPOST(['jornada'])) {
                $jornada = $this->getPOST('jornada');
            }
            if ($this->existPOST(['minimos'])) {
                $estudiosMinimo = $this->getPOST('minimos');
            }
            if ($this->existPOST(['descripcion'])) {
                $descripcion = $this->getPOST('descripcion');
            }
            if ($this->existPOST(['salario'])) {
                $salario = $this->getPOST('salario');
            }

            $oferta = new OfertaModel();
            $oferta->setNombre($nombre);
            $oferta->setJornada($jornada);
            $oferta->setEstudioMinimo($estudiosMinimo);
            $oferta->setDescripcion($descripcion);
            $oferta->setSalario($salario);
            $oferta->setEmpresaId($empresaId);

            $oferta->save();
            /*
                $usuario = new TalentoModel();
                $usuario->setNombre($nombre);
                $usuario->setApellidos($apellidos);
                $usuario->setEmail($email);
                $usuario->setEstudiosNombre($estudios_nombre);
                $usuario->setEstudiosTipo($estudios_tipo);
                $usuario->setEstudiosMedia($estudios_media);
                $usuario->setUserId($this->user->getId());

                $usuario->update();
            }
            if ($empresa->getEmpresaId()) {
                $usuario = new EmpresaModel();
                $usuario->setNombre($nombre);
                $usuario->setApellidos($apellidos);
                $usuario->setEmail($email);
                $usuario->setSector($sector);
                $usuario->setLocalizacion($localizacion);
                $usuario->setWeb($web);
                $usuario->setUserId($this->user->getId());
                $usuario->update();
            }
            $this->redirect('nuevaOferta');
            */
        }
    }
}
