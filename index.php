<?php
error_reporting(E_ALL | E_STRICT);

ini_set('ignore_repeated_errors',TRUE);

ini_set('display_errors',FALSE);

ini_set('log_errors',TRUE);

ini_set("error_log", "php-error.log");
error_log('------------------------------------');
error_log('Inicio de Aplicacion Web');

require_once 'libs/database.php';
require_once 'libs/controller.php';
require_once 'libs/model.php';
require_once 'libs/view.php';
require_once 'libs/app.php';

require_once 'classes/session.php';
require_once 'classes/sessioncontroller.php';
require_once 'classes/errorMessages.php';
require_once 'classes/successMessages.php';

require_once 'config/config.php';

require_once 'models/loginmodel.php';
require_once 'models/usermodel.php';
require_once 'models/empresamodel.php';
require_once 'models/talentomodel.php';
require_once 'models/ofertamodel.php';
require_once 'models/aptitudmodel.php';
require_once 'models/aptitudtalentomodel.php';
require_once 'models/empresatalentomodel.php';
require_once 'models/talentoempresamodel.php';

$app = new App();
?>