document.addEventListener("DOMContentLoaded", function () {
  sidebarActive();
});

function sidebarActive() {
  const direccion = window.location.href.split("/");
  const sidebar = document.getElementById("mm-sidebar-toggle");

  if (direccion.includes("dashboard")) {
    if (sidebar.children[0].classList.contains("active")) {
        sidebar.children[0].classList.remove("active");
      } else sidebar.children[0].classList.add("active");
  }

  if (direccion.includes("empresas") || direccion.includes("talentos")) {
    if (sidebar.children[1].classList.contains("active")) {
        sidebar.children[1].classList.remove("active");
      } else sidebar.children[1].classList.add("active");
  }
  
  if (direccion.includes("ofertas") || direccion.includes("nuevaOferta")) {
    if (sidebar.children[2].classList.contains("active")) {
        sidebar.children[2].classList.remove("active");
      } else sidebar.children[2].classList.add("active");
  }

  if (direccion.includes("misIntereses")) {
    if (sidebar.children[3].classList.contains("active")) {
        sidebar.children[3].classList.remove("active");
      } else sidebar.children[3].classList.add("active");
  }

  if (direccion.includes("interesadas")) {
    if (sidebar.children[4].classList.contains("active")) {
        sidebar.children[4].classList.remove("active");
      } else sidebar.children[4].classList.add("active");
  }

  if (direccion.includes("interesMutuo")) {
    if (sidebar.children[5].classList.contains("active")) {
        sidebar.children[5].classList.remove("active");
      } else sidebar.children[5].classList.add("active");
  }
  if (direccion.includes("configuracion")) {
    if (sidebar.children[6].classList.contains("active")) {
        sidebar.children[6].classList.remove("active");
      } else sidebar.children[6].classList.add("active");
  }
}
