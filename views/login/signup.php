<?php include './views/header.php'; ?>

<body class="">

    <div id="loading" style="display: none;">
        <div id="loading-center">
        </div>
    </div>
    <div class="wrapper">
        <section class="login-content">
            <div class="container h-100">
                <div class="row align-items-center justify-content-center h-100">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="auth-logo">
                                    <img src="/public/img/logo-talentcan.png" class="img-fluid rounded-normal" alt="logo">
                                </div>
                                <h2 class="mb-2 text-center">Registro</h2>
                                <p class="text-center">Para poder acceder necesitamos algunos datos personales.</p>
                                <?php $this->showMessages(); ?>
                                <form action="<?php echo constant('URL'); ?>/signup/newUser" method="POST">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input class="form-control" name="nombre" id="nombre" type="text" placeholder=" ">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Apellidos</label>
                                                <input class="form-control" name="apellidos" id="apellidos" type="text" placeholder=" ">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input class="form-control" name="email" id="email" type="email" placeholder=" ">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Contraseña</label>
                                                <input class="form-control" id="password" name="password" type="password" placeholder=" ">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Rol</label>
                                                <select class="form-control" id="rol" name="rol" type="select">
                                                    <option value="talento">Talento</option>
                                                    <option value="empresa">Empresa</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span>¿Ya tienes una cuenta?
                                            <a href="<?php echo constant('URL'); ?>">Inicia Sesión</a>
                                        </span>
                                        <input class="btn btn-primary" type="submit" value="Registrar">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php include './views/footer.php'; ?>