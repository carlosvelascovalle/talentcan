<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TALENTCAN</title>
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>/public/css/index.css">
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>/public/css/backend-plugin.css">
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>/public/css/default.css">
</head>

<body class="">
    <div class="wrapper">
        <section class="login-content">
            <div class="container h-100">
                <div class="row align-items-center justify-content-center h-100">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="auth-logo">
                                    <img src="<?php echo constant('URL'); ?>/public/img/logo-talentcan.png" class="img-fluid rounded-normal" alt="logo">
                                </div>
                                <h2 class="mb-2 text-center">Acceso</h2>
                                <p class="text-center">Si ya estas registrado con nosotros accede desde aquí.
                                </p>
                                <p class="text-center"><?php $this->showMessages();?></p>
                                <form action="<?php echo constant('URL');?>/login/authenticate" method="POST">
                                <div class="text-center"><?php (isset($this->errorMessage))?  $this->errorMessage : '' ?></div>    
                                <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input class="form-control" type="email" name="username" id="username" placeholder="usuario@mail.es" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Contraseña</label>
                                                <input class="form-control" type="password" name="password" id="password" placeholder="********" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span>¿No tienes cuenta? <a href="<?php echo constant('URL'); ?>/signup" class="text-primary">Registrate</a></span>
                                        <input class="btn btn-primary" type="submit" value="Iniciar Sesión">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php include './views/footer.php'; ?>