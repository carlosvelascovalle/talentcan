<?php
include './views/header.php';
$user = $this->d['user'];
$talento = $this->d['talento'];
$empresa = $this->d['empresa'];
$aptitudesTodas = $this->d['aptitudesTodas'];
$aptitudes = $this->d['aptitudes'];
$aptitudTalento = $this->d['aptitudTalento'];

if ($talento->getTalentoId()) {
    $usuario = $talento;
}
if ($empresa->getEmpresaId()) {
    $usuario = $empresa;
}
?>
<div class="col-lg-12 mb-3">
    <div class="row">
        <div class="col-lg-12">
            <form method="post" action="<?php echo constant('URL'); ?>/nuevaOferta/crear">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input class='form-control' type='text' id='nombre' name='nombre' required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Jornada</label>
                                <select class='form-control' type="select" name="jornada" id="jornada" required>
                                    <option value="Completa">Completa</option>
                                    <option value="Media">Media</option>
                                    <option value="Teletrabajo">Teletrabajo</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Estudios Minimos</label>
                                <select class='form-control' type="select" name="minimos" id="minimos" required>
                                    <option value="Secundaria">Secundaria</option>
                                    <option value="Bachillerato">Bachillerato</option>
                                    <option value="FP-I">FP-I</option>
                                    <option value="FP-II">FP-II</option>
                                    <option value="Grado">Grado</option>
                                    <option value="Post-Grado">Post-Grado</option>
                                    <option value="Maestria">Maestria</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Salario Mensual</label>
                                <input class='form-control' type="number" min="0" max="10000" step="1" name="salario" id="salario" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label>Descripción</label>
                            <textarea class='form-control' type='text' id='descripcion' name='descripcion' rows='13' required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-10">
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <input class="btn btn-primary" type="submit" name="guardar" value="Guardar">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
include './views/footer.php';
?>