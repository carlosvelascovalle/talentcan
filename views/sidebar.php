<div class="g-sidebar">
        <ul class="g-sidebar-navlist">
            <li class="g-sidebar-list">
                <a href="/dashboard">
                    <span class="g-sidebar-icon">
                        <img class="g-sidebar-menu" src="<?php echo constant('URL'); ?>/public/img/dashboard/rocket-dashboard.svg">
                    </span>
                    <span class="g-sidebar-title">Dashboard</span>
                </a>
            </li>
            <li class="g-sidebar-list">
                <a href="/buscar">
                    <span class="g-sidebar-icon">
                        <img class="g-sidebar-menu" src="<?php echo constant('URL'); ?>/public/img/dashboard/radar.svg">
                    </span>
                    <span class="g-sidebar-title">Buscar</span>
                </a>
            </li>
            <li class="g-sidebar-list">
                <a href="/seleccion">
                    <span class="g-sidebar-icon">
                        <img class="g-sidebar-menu" src="<?php echo constant('URL'); ?>/public/img/dashboard/focus-darts.svg">
                    </span>
                    <span class="g-sidebar-title">Seleccionadas</span>
                </a>
            </li>
            <li class="g-sidebar-list">
                <a href="/interes">
                    <span class="g-sidebar-icon">
                        <img class="g-sidebar-menu" src="<?php echo constant('URL'); ?>/public/img/dashboard/best-employ.svg">
                    </span>
                    <span class="g-sidebar-title">Interesadas</span>
                </a>
            </li>
            <li class="g-sidebar-list">
                <a href="/configuracion">
                    <span class="g-sidebar-icon">
                        <img class="g-sidebar-menu" src="<?php echo constant('URL'); ?>/public/img/dashboard/settings.svg">
                    </span>
                    <span class="g-sidebar-title">Configuración</span>
                </a>
            </li>
            <li class="g-sidebar-list">
                <a href="#">
                    <span class="g-sidebar-icon">
                        <img class="g-sidebar-menu" src="<?php echo constant('URL'); ?>/public/img/dashboard/log-out.svg">
                    </span>
                    <span class="g-sidebar-title">Salir</span>
                </a>
            </li>
        </ul>
</div>