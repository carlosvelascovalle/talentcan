<?php
include './views/header.php';
$user = $this->d['user'];
$empresas = $this->d['empresas'];
$talentos = $this->d['talentos'];
$ofertas = $this->d['ofertas'];

?>
<div class="col-lg-12 mb-3">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card card-block card-stretch card-height rule-box">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Ofertas Mejor Pagadas</h4>
                    </div>
                    <div class="card-header-toolbar d-flex align-items-center">
                        <a href="/ofertas" class="btn btn-outline-primary view-more">Más Ofertas</a>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="perfomer-lists m-0 p-0">
                        <?php for ($i = 0; $i < 5; $i++) { ?>
                            <li class="d-flex mb-3 align-items-center">
                                <div class="top-block-one d-flex align-items-center justify-content-between">
                                    <div class="bg-primary icon mm-icon-box mr-0 rounded">
                                        <svg class="svg-icon fill-none" width="35" height="35" id="d-1-g" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3.055 11H5a2 2 0 012 2v1a2 2 0 002 2 2 2 0 012 2v2.945M8 3.935V5.5A2.5 2.5 0 0010.5 8h.5a2 2 0 012 2 2 2 0 104 0 2 2 0 012-2h1.064M15 20.488V18a2 2 0 012-2h3.064M21 12a9 9 0 11-18 0 9 9 0 0118 0z" style="stroke-dasharray: 101, 121; stroke-dashoffset: 0;">
                                            </path>
                                        </svg>
                                    </div>
                                </div>
                                <div class="media-support-info ml-3">
                                    <h5>
                                        <?php echo $ofertas[$i]->getNombre(); ?>
                                    </h5>
                                    <p class="mb-0 font-size-14 text-primary">
                                        <?php echo $ofertas[$i]->getJornada(); ?>
                                    </p>
                                </div>
                                <div class="card-header-toolbar d-flex align-items-center">
                                    <h4 class="text-danger"><b>
                                            <?php echo $ofertas[$i]->getSalario(); ?> €
                                        </b></h4>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="card card-block card-stretch card-height">
        <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title">Empresas</h4>
            </div>
            <div class="card-header-toolbar d-flex align-items-center">
                <?php if ($user->getRole() == 'talento') { ?>
                    <a onclick="location.assign('<?php echo constant('URL'); ?>/empresas');" class="btn btn-outline-primary view-more">Ver Más</a>
                <?php }?>
            </div>
        </div>
        <div class="card-body">
            <ul class="list-inline p-0 m-0">
                <?php for ($i = 1; $i < 5; $i++) { ?>
                    <li class="media align-items-center pt-1 pb-1">
                        <a href="JavaScript:Void(0);">
                            <div class="bg-primary icon mm-icon-box mr-0 rounded">
                                <svg xmlns="http://www.w3.org/2000/svg" stroke="currentColor" fill="none" viewBox="0 0 24 24" width="35" height="35">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 19h3v-6.058L8 9.454l-4 3.488V19h3v-4h2v4zm12 2H3a1 1 0 0 1-1-1v-7.513a1 1 0 0 1 .343-.754L6 8.544V4a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1zm-5-10v2h2v-2h-2zm0 4v2h2v-2h-2zm0-8v2h2V7h-2zm-4 0v2h2V7h-2z" />
                                </svg>
                            </div>
                        </a>
                        <div class="media-body ml-3">
                            <div class="d-flex justify-content-between">
                                <h5><?php echo $empresas[$i]->getNombre(); ?></h5>
                                <span class="text-danger font-size-14"><?php echo $empresas[$i]->getEmail(); ?></span>
                            </div>
                            <span class="text-primary"><?php echo $empresas[$i]->getSector(); ?></span>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="card card-block card-stretch card-height">
        <div class="card-header d-flex justify-content-between">
            <div class="header-title">
                <h4 class="card-title">Talento</h4>
            </div>
            <div class="card-header-toolbar d-flex align-items-center">
                <?php if ($user->getRole() == 'empresa') { ?>
                    <a onclick="location.assign('<?php echo constant('URL'); ?>/talentos');" class="btn btn-outline-primary view-more">Ver Más</a>
                <?php }?>
            </div>
        </div>
        <div class="card-body">
            <ul class="list-inline p-0 m-0">
                <?php for ($i = 1; $i < 5; $i++) { ?>
                    <li class="media align-items-center pt-1 pb-1">
                        <a href="JavaScript:Void(0);">
                            <div class="bg-primary icon mm-icon-box mr-0 rounded">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" stroke="currentColor" viewBox="0 0 24 24" width="35" height="35">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.33 15.929A13.064 13.064 0 0 1 5 13c0-5.088 2.903-9.436 7-11.182C16.097 3.564 19 7.912 19 13c0 1.01-.114 1.991-.33 2.929l2.02 1.796a.5.5 0 0 1 .097.63l-2.458 4.096a.5.5 0 0 1-.782.096l-2.254-2.254a1 1 0 0 0-.707-.293H9.414a1 1 0 0 0-.707.293l-2.254 2.254a.5.5 0 0 1-.782-.096l-2.458-4.095a.5.5 0 0 1 .097-.631l2.02-1.796zM12 13a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" />
                                </svg>
                            </div>
                        </a>
                        <div class="media-body ml-3">
                            <h5><?php echo $talentos[$i]->getNombre(); ?> <?php echo $talentos[$i]->getApellidos(); ?></h5>
                            <p class="mb-0 text-primary"><?php echo $talentos[$i]->getEstudiosNombre(); ?></p>
                        </div>
                        <p class=" mb-0 text-danger"><b><?php echo $talentos[$i]->getEstudiosTipo(); ?></b></p>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<?php include './views/footer.php'; ?>