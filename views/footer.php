</div>
</div>
</div>
</div>
<footer class="mm-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item"><a href="#">Politica de Privacidad</a>
                    </li>
                    <li class="list-inline-item"><a href="#">Terminos de Uso</a></li>
                </ul>
            </div>
            <div class="col-lg-6 text-right">
                <span class="mr-1">
                    Copyright
                    <script>
                        document.write(new Date().getFullYear())
                    </script>© <a href="https://meetmighty.com/dashboards/simpled/html/backend/index.html" class="">SimpleD</a>
                    Todos los derechos reservados.
                </span>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo constant('URL'); ?>/public/js/backend-bundle.min.js"></script>
<script src="<?php echo constant('URL'); ?>/public/js/customizer.js"></script>
<script src="<?php echo constant('URL'); ?>/public/js/app.js"></script>
<script src="<?php echo constant('URL'); ?>/public/js/index.js"></script>
<script src="<?php echo constant('URL'); ?>/public/js/peticion.js"></script>
<script>
    function redireccion($route) {
        $.ajax({
            type: "POST",
            url: 'localhost/talentcant/'.$route,
            data: {
                action: 'configuracion'
            },
            success: function(datos) {
                if (datos == 'configuracion')
                    location.href = "http://www.pagina1.com/configuracion";
            }
        });
    }
</script>
<svg id="SvgjsSvg1282" width="2" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" style="overflow: hidden; top: -100%; left: -100%; position: absolute; opacity: 0;">
    <defs id="SvgjsDefs1283"></defs>
    <polyline id="SvgjsPolyline1284" points="0,0"></polyline>
    <path id="SvgjsPath1285" d="M0 0 "></path>
</svg>
</body>

</html>