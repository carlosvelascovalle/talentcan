<?php
include './views/header.php';
$user = $this->d['user'];
$empresas = $this->d['empresas'];
$talentos = $this->d['talentos'];
$ofertas = $this->d['ofertas'];
$empresasInteresadas = $this->d['empresasInteresadas'];

?>
<div class="col-lg-12 mb-3">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card card-block card-stretch card-height rule-box">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Empresas</h4>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="perfomer-lists m-0 p-0" style="list-style:none;">
                        <?php foreach ($empresas as $empresa) { ?>
                            <li class="mb-3">
                                <form action="<?php echo constant('URL'); ?>/empresas/interes" method="POST" class="d-flex mb-3 align-items-center">
                                    <div class="top-block-one d-flex align-items-center justify-content-between">
                                        <div class="bg-primary icon mm-icon-box mr-0 rounded">
                                            <svg class="svg-icon fill-none" width="35" height="35" id="d-1-g" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3.055 11H5a2 2 0 012 2v1a2 2 0 002 2 2 2 0 012 2v2.945M8 3.935V5.5A2.5 2.5 0 0010.5 8h.5a2 2 0 012 2 2 2 0 104 0 2 2 0 012-2h1.064M15 20.488V18a2 2 0 012-2h3.064M21 12a9 9 0 11-18 0 9 9 0 0118 0z" style="stroke-dasharray: 101, 121; stroke-dashoffset: 0;">
                                                </path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="media-support-info ml-3">
                                        <h5>
                                            <?php echo $empresa->getNombre(); ?>
                                        </h5>
                                        <p class="mb-0 font-size-14 text-primary">
                                            <?php echo $empresa->getWeb(); ?>
                                        </p>
                                    </div>
                                    <div class="col-lg-3 card-header-toolbar d-flex align-items-center">
                                        <?php
                                        if (in_array($empresa,$empresasInteresadas)) {
                                        ?>
                                            <button type="submit" class="mt-2 btn btn-primary btn-block">
                                                Solicitud Enviada
                                            </button>
                                        <?php
                                        } else {
                                        ?>
                                            <button type="submit" class="mt-2 btn btn-success btn-block" name="empresaId" value="<?php echo $empresa->getEmpresaId(); ?>">
                                                Enviar Solicitud
                                            </button>
                                        <?php
                                        }
                                        ?>

                                    </div>
                                </form>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include './views/footer.php'; ?>