<?php
/*
$tabla = "";
$ofertaModel = new OfertaModel();
$buscarOfertas = $ofertaModel->getAllOrderBySalario();
var_dump($_POST['ofertas']);

if (isset($_POST['ofertas'])) {
    $buscarOfertas = $ofertaModel->buscarOferta($_POST['ofertas']);
}

if (count($buscarOfertas) > 0) {
    $tabla .=
        '<table class="table">
                    <tr class="bg-primary">
                    <td>ID ALUMNO</td>
                    <td>jornada</td>
                    <td>estudio_minimo</td>
                    <td>salario</td>
                    <td>descripcion</td>
                    </tr>';

    foreach ($buscarOfertas as $oferta) {
        $tabla .=
            '<tr>
            <td>' . $oferta->getNombre() . '</td>
            <td>' . $oferta->getJornada() . '</td>
            <td>' . $oferta->getEstudioMinimo() . '</td>
            <td>' . $oferta->getSalario() . '</td>
            <td>' . $oferta->getDescripcion() . '</td>
            </tr>';
    }

    $tabla .= '</table>';
} else {
    $tabla = "No se encontraron coincidencias con sus criterios de búsqueda.";
}


echo $tabla;
*/

/////// CONEXIÓN A LA BASE DE DATOS /////////
$host = 'localhost';
$basededatos = 'h177509_talentcan';
$usuario = 'h177509_carlos';
$contraseña = 'qSWx-5k38VftD5La';

$conexion = new mysqli($host, $usuario, $contraseña, $basededatos);

//////////////// VALORES INICIALES ///////////////////////

$tabla = "";
$query = "SELECT * FROM ofertas ORDER BY salario";

///////// LO QUE OCURRE AL TECLEAR SOBRE EL INPUT DE BUSQUEDA ////////////
if (isset($_POST['ofertas'])) {
    $busqueda = $conexion->real_escape_string($_POST['ofertas']);
    $query = "SELECT * FROM oferta WHERE nombre LIKE '%" . $busqueda . "%' OR jornada LIKE '%" . $busqueda . "%' OR estudio_minimo LIKE '%" . $busqueda . "%' OR descripcion LIKE '%" . $busqueda . "%' OR salario LIKE '%" . $busqueda . "%'";
}

$buscarOfertas = $conexion->query($query);
if ($buscarOfertas) {
    while ($filaOfertas = $buscarOfertas->fetch_assoc()) {
        $tabla .=
            '<li class="mb-3 d-flex mb-3 align-items-center">
            <form action="http://localhost:80/talentcan/ofertas/interes" method="POST" class="d-flex mb-3 align-items-center">'
            . '<div class="top-block-one d-flex align-items-center justify-content-between">
            <div class="bg-primary icon mm-icon-box mr-0 rounded">
            <svg class="svg-icon fill-none" width="35" height="35" id="d-1-g" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 12l3-3 3 3 4-4M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" style="stroke-dasharray: 101, 121; stroke-dashoffset: 0;">
            </path>
            </svg>
            </div>
            </div>
            <div class="media-support-info ml-3">
            <h5>'
            . $filaOfertas['nombre'] . '</h5>
            <p class="mb-1 font-size-14 text-danger">'
            . $filaOfertas['salario'] . ' €/mes </b> | <b>Jornada '
            . $filaOfertas['jornada'] . '</b> | <b>Estudios Minimos: '
            . $filaOfertas['estudio_minimo'] . '</p>
            <p class="mb-0 font-size-14 text-primary">'
            . $filaOfertas['descripcion'] . '</p>
            </div>
            </form>
            </li>';
    }
} else {
    $tabla = "No se encontraron coincidencias con sus criterios de búsqueda.";
}


echo $tabla;
