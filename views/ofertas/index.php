<?php
include './views/header.php';

$ofertasInteres = [];
$user = $this->d['user'];
$empresas = $this->d['empresas'];
$talentos = $this->d['talentos'];
$ofertas = $this->d['ofertas'];
$ofertaModel = $this->d['ofertaModel'];
$getAllBySalario = $this->d['getAllBySalario'];

$empresasInteresadas = $this->d['empresasInteresadas'];
foreach ($empresasInteresadas as $oferta) {
    array_push($ofertasInteres, $oferta->getEmpresaId());
}
?>

<div class="col-lg-12 mb-3">

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="searchbox">
                <a class="search-link" href="#"><i class="ri-search-line"></i></a>
                <input type="text" class="text search-input" name="busqueda" id="busqueda" placeholder="Buscar...">
            </div>
            <div class="card card-block card-stretch card-height rule-box">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Buscador de Ofertas</h4>
                    </div>
                </div>
                <div class="card-body">
                    <ul id="tabla_resultado" class="perfomer-lists m-0 p-0" style="list-style:none;">
                        </section>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card card-block card-stretch card-height rule-box">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Ofertas</h4>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="perfomer-lists m-0 p-0" style="list-style:none;">
                        <?php foreach ($ofertas as $oferta) { ?>
                            <li class="mb-3">
                                <form action="<?php echo constant('URL'); ?>/ofertas/interes" method="POST" class="d-flex mb-3 align-items-center">
                                    <div class="top-block-one d-flex align-items-center justify-content-between">
                                        <div class="bg-primary icon mm-icon-box mr-0 rounded">
                                            <svg class="svg-icon fill-none" width="35" height="35" id="d-1-g" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 12l3-3 3 3 4-4M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" style="stroke-dasharray: 101, 121; stroke-dashoffset: 0;">
                                                </path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="media-support-info ml-3">
                                        <h5>
                                            <?php echo $oferta->getNombre(); ?>
                                        </h5>
                                        <p class="mb-1 font-size-14 text-danger">
                                            <b><?php echo $oferta->getSalario(); ?> €/mes </b> | <b>Jornada <?php echo $oferta->getJornada(); ?></b> | <b>Estudios Minimos: <?php echo $oferta->getEstudioMinimo(); ?></b>
                                        </p>
                                        <p class="mb-0 font-size-14 text-primary mr-5">
                                            <?php echo $oferta->getDescripcion(); ?>
                                        </p>
                                    </div>
                                    <div class="col-lg-3 card-header-toolbar d-flex align-items-center">
                                        <?php
                                        if (in_array($oferta->getEmpresaId(), $ofertasInteres)) {
                                        ?>
                                            <button type="submit" class="mt-2 btn btn-primary btn-block">
                                                <i class="ri-bill-fill"></i>
                                                Solicitud Enviada
                                            </button>
                                        <?php
                                        } else {

                                        ?>
                                            <button type="submit" class="mt-2 btn btn-success btn-block" name="empresaId" value="<?php echo $oferta->getEmpresaId(); ?>">
                                                <i class="ri-bill-fill"></i>
                                                Enviar Solicitud
                                            </button>
                                        <?php }
                                        ?>
                                    </div>
                                </form>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include './views/footer.php'; ?>