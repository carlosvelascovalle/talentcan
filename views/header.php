<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TALENTCAN</title>
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>/public/css/index.css">
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>/public/css/backend-plugin.css">
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>/public/css/default.css">
</head>
<?php
$user = $this->d['user'];
$usuariosTotales = $this->d['usuariosTotales'];
$empresasTotales = $this->d['empresasTotales'];
$talentosTotales = $this->d['talentosTotales'];
$ofertasTotales = $this->d['ofertasTotales'];

function closeSession()
{
    ob_start();
    error_log('Session::closeSession()');
    session_unset();
    session_destroy();
    header('Location: login.php');
    ob_end_flush();
}

?>

<body>
    <div class="wrapper">
        <div class="mm-sidebar sidebar-default ">
            <div class="mm-sidebar-logo d-flex align-items-center justify-content-between">
                <a onclick="location.assign('<?php echo constant('URL'); ?>/dashboard');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                    <img src="public/img/logo-talentcan.png" class="img-fluid rounded-normal light-logo" alt="logo">
                    <img src="public/img/logo-talentcan.png" class="img-fluid rounded-normal darkmode-logo d-none" alt="logo">
                </a>
                <div class="side-menu-bt-sidebar">
                    <i class="wrapper-menu">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 4h18v2H3V4zm0 7h18v2H3v-2zm0 7h18v2H3v-2z" />
                        </svg>
                    </i>
                </div>
            </div>
            <div class="data-scrollbar" data-scroll="1" data-scrollbar="true" tabindex="-1" style="overflow: hidden; outline: none;">
                <div class="scroll-content">
                    <nav class="mm-sidebar-menu">
                        <ul id="mm-sidebar-toggle" class="side-menu">
                            <li class="">
                                <a onclick="location.assign('<?php echo constant('URL'); ?>/dashboard');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                                    <i class="">
                                        <svg class="svg-icon" id="mm-dash" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" style="stroke-dasharray: 77, 97; stroke-dashoffset: 0;"></path>
                                        </svg>
                                    </i>
                                    <span class="ml-2">Dashboard</span>
                                </a>
                            </li>
                            <li class="">
                                <?php
                                if ($user->getRole() == 'empresa') {
                                ?>
                                    <a onclick="location.assign('<?php echo constant('URL'); ?>/talentos');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                                        <i>
                                            <svg class="svg-icon" id="mm-form-1" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path d="M21 19h2v2H1v-2h2V4a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1v15h4v-8h-2V9h3a1 1 0 0 1 1 1v9zM5 5v14h8V5H5zm2 6h4v2H7v-2zm0-4h4v2H7V7z" />
                                            </svg>
                                        </i>
                                        <span class="ml-2">Talentos</span>
                                    </a>
                                <?php
                                }
                                if ($user->getRole() == 'talento') {
                                ?>
                                    <a onclick="location.assign('<?php echo constant('URL'); ?>/empresas');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                                        <i>
                                            <svg class="svg-icon" id="mm-form-1" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path d="M21 19h2v2H1v-2h2V4a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1v15h4v-8h-2V9h3a1 1 0 0 1 1 1v9zM5 5v14h8V5H5zm2 6h4v2H7v-2zm0-4h4v2H7V7z" />
                                            </svg>
                                        </i>
                                        <span class="ml-2">Empresas</span>
                                    </a>
                                <?php
                                }
                                ?>
                            </li>
                            <li class="">
                                <?php
                                if ($user->getRole() == 'empresa') {
                                ?>
                                    <a onclick="location.assign('<?php echo constant('URL'); ?>/nuevaOferta');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                                        <i>
                                            <svg class="svg-icon" id="mm-form-1" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path d="M7 5V2a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v3h4a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h4zm2 8H4v6h16v-6h-5v3H9v-3zm11-6H4v4h5V9h6v2h5V7zm-9 4v3h2v-3h-2zM9 3v2h6V3H9z" />
                                            </svg>
                                        </i>
                                        <span class="ml-2">Nueva Oferta</span>
                                    </a>
                                <?php
                                }
                                if ($user->getRole() == 'talento') {
                                ?>
                                    <a onclick="location.assign('<?php echo constant('URL'); ?>/ofertas');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                                        <i>
                                            <svg class="svg-icon" id="mm-form-1" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path d="M7 5V2a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v3h4a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h4zm2 8H4v6h16v-6h-5v3H9v-3zm11-6H4v4h5V9h6v2h5V7zm-9 4v3h2v-3h-2zM9 3v2h6V3H9z" />
                                            </svg>
                                        </i>
                                        <span class="ml-2">Ofertas</span>
                                    </a>
                                <?php
                                }
                                ?>
                            </li>
                            </li>
                            <li class="">
                                <a onclick="location.assign('<?php echo constant('URL'); ?>/misIntereses');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                                    <i>
                                        <svg class="svg-icon" id="mm-table-1" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path d="M17 2v2h3.007c.548 0 .993.445.993.993v16.014c0 .548-.445.993-.993.993H3.993C3.445 22 3 21.555 3 21.007V4.993C3 4.445 3.445 4 3.993 4H7V2h10zM7 6H5v14h14V6h-2v2H7V6zm2 10v2H7v-2h2zm0-3v2H7v-2h2zm0-3v2H7v-2h2zm6-6H9v2h6V4z" />
                                        </svg>
                                    </i>
                                    <span class="ml-2">Mis Intereses</span>
                                </a>
                            </li>
                            <li class="">
                                <a onclick="location.assign('<?php echo constant('URL'); ?>/interesadas');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                                    <i>
                                        <svg class="svg-icon" id="mm-icon-1" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path d="M5 9a1 1 0 0 1 1 1 6.97 6.97 0 0 1 4.33 1.5h2.17c1.333 0 2.53.58 3.354 1.5H19a5 5 0 0 1 4.516 2.851C21.151 18.972 17.322 21 13 21c-2.79 0-5.15-.603-7.06-1.658A.998.998 0 0 1 5 20H2a1 1 0 0 1-1-1v-9a1 1 0 0 1 1-1h3zm1.001 3L6 17.022l.045.032C7.84 18.314 10.178 19 13 19c3.004 0 5.799-1.156 7.835-3.13l.133-.133-.12-.1a2.994 2.994 0 0 0-1.643-.63L19 15h-2.111c.072.322.111.656.111 1v1H8v-2l6.79-.001-.034-.078a2.501 2.501 0 0 0-2.092-1.416L12.5 13.5H9.57A4.985 4.985 0 0 0 6.002 12zM4 11H3v7h1v-7zm14-6a3 3 0 1 1 0 6 3 3 0 0 1 0-6zm0 2a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-7-5a3 3 0 1 1 0 6 3 3 0 0 1 0-6zm0 2a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
                                        </svg>
                                    </i>
                                    <?php
                                    if ($user->getRole() == 'empresa') {
                                    ?>
                                        <span class="ml-2">Talento Interesado</span>
                                    <?php
                                    }
                                    if ($user->getRole() == 'talento') {
                                    ?>
                                        <span class="ml-2">Empresas Interesadas</span>
                                    <?php
                                    }
                                    ?>
                                </a>
                            <li class="">
                                <a onclick="location.assign('<?php echo constant('URL'); ?>/interesMutuo');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                                    <i>
                                        <svg class="svg-icon" id="mm-icon-1" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-5-7h9v2h-4v3l-5-5zm5-4V6l5 5H8V9h4z" />
                                        </svg>
                                    </i>
                                    <span class="ml-2">Interes Mutuo</span>
                                </a>
                            </li>
                            <li class="">
                                <a onclick="location.assign('<?php echo constant('URL'); ?>/configuracion');" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false" style="cursor:pointer;">
                                    <i>
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                            <path d="M2.213 14.06a9.945 9.945 0 0 1 0-4.12c1.11.13 2.08-.237 2.396-1.001.317-.765-.108-1.71-.986-2.403a9.945 9.945 0 0 1 2.913-2.913c.692.877 1.638 1.303 2.403.986.765-.317 1.132-1.286 1.001-2.396a9.945 9.945 0 0 1 4.12 0c-.13 1.11.237 2.08 1.001 2.396.765.317 1.71-.108 2.403-.986a9.945 9.945 0 0 1 2.913 2.913c-.877.692-1.303 1.638-.986 2.403.317.765 1.286 1.132 2.396 1.001a9.945 9.945 0 0 1 0 4.12c-1.11-.13-2.08.237-2.396 1.001-.317.765.108 1.71.986 2.403a9.945 9.945 0 0 1-2.913 2.913c-.692-.877-1.638-1.303-2.403-.986-.765.317-1.132 1.286-1.001 2.396a9.945 9.945 0 0 1-4.12 0c.13-1.11-.237-2.08-1.001-2.396-.765-.317-1.71.108-2.403.986a9.945 9.945 0 0 1-2.913-2.913c.877-.692 1.303-1.638.986-2.403-.317-.765-1.286-1.132-2.396-1.001zM4 12.21c1.1.305 2.007 1.002 2.457 2.086.449 1.085.3 2.22-.262 3.212.096.102.195.201.297.297.993-.562 2.127-.71 3.212-.262 1.084.45 1.781 1.357 2.086 2.457.14.004.28.004.42 0 .305-1.1 1.002-2.007 2.086-2.457 1.085-.449 2.22-.3 3.212.262.102-.096.201-.195.297-.297-.562-.993-.71-2.127-.262-3.212.45-1.084 1.357-1.781 2.457-2.086.004-.14.004-.28 0-.42-1.1-.305-2.007-1.002-2.457-2.086-.449-1.085-.3-2.22.262-3.212a7.935 7.935 0 0 0-.297-.297c-.993.562-2.127.71-3.212.262C13.212 6.007 12.515 5.1 12.21 4a7.935 7.935 0 0 0-.42 0c-.305 1.1-1.002 2.007-2.086 2.457-1.085.449-2.22.3-3.212-.262-.102.096-.201.195-.297.297.562.993.71 2.127.262 3.212C6.007 10.788 5.1 11.485 4 11.79c-.004.14-.004.28 0 .42zM12 15a3 3 0 1 1 0-6 3 3 0 0 1 0 6zm0-2a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                        </svg>
                                    </i>
                                    <span class="ml-2">Configuración</span>
                                </a>
                            </li>
                            <li class="lg-12">
                                <form action="views/closeSession.php" name="sesionDestroy" class="d-flex align-items-center justify-content-center lg-12">
                                    <button type="submit" class="btn btn-block btn-outline-danger mt-5 pl-2 pr-2 lg-12"> Salir </button>
                                </form>
                            </li>
                        </ul>
                    </nav>
                    <div class="pt-5 pb-2"></div>
                </div>
                <div class="scrollbar-track scrollbar-track-x" style="display: none;">
                    <div class="scrollbar-thumb scrollbar-thumb-x" style="width: 260px; transform: translate3d(0px, 0px, 0px);"></div>
                </div>
                <div class="scrollbar-track scrollbar-track-y" style="display: block;">
                    <div class="scrollbar-thumb scrollbar-thumb-y" style="height: 411.755px; transform: translate3d(0px, 0px, 0px);"></div>
                </div>
            </div>
        </div>
        <div class="mm-top-navbar">
            <div class="mm-navbar-custom">
                <nav class="navbar navbar-expand-lg navbar-light p-0">
                    <div class="mm-navbar-logo d-flex align-items-center justify-content-between">
                        <i class="wrapper-menu" style="cursor:pointer;">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 4h18v2H3V4zm0 7h18v2H3v-2zm0 7h18v2H3v-2z" />
                            </svg>
                        </i>
                        <a href="/backend/index.html" class="ml-3 header-logo">
                            <img src="public/img/astronauta.png" class="img-fluid rounded-normal" alt="logo">
                            <h4 class="ml-1 mr-3"><b>TalenCan</b></h4>
                        </a>
                    </div>
                    <div class="mm-search-bar device-search m-auto">
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="change-mode">
                            <div class="custom-control custom-switch custom-switch-icon custom-control-inline">
                                <div class="custom-switch-inner">
                                    <p class="mb-0"> </p>
                                    <input type="checkbox" class="custom-control-input" id="dark-mode" data-active="true">
                                    <label class="custom-control-label" for="dark-mode" data-mode="toggle">
                                        <span class="switch-icon-left">
                                            <svg class="svg-icon" id="h-moon" height="20" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z" style="stroke-dasharray: 57, 77; stroke-dashoffset: 0;"></path>
                                            </svg>
                                        </span>
                                        <span class="switch-icon-right">
                                            <svg class="svg-icon" id="h-sun" height="20" width="20" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 3v1m0 16v1m9-9h-1M4 12H3m15.364 6.364l-.707-.707M6.343 6.343l-.707-.707m12.728 0l-.707.707M6.343 17.657l-.707.707M16 12a4 4 0 11-8 0 4 4 0 018 0z" style="stroke-dasharray: 34, 54; stroke-dashoffset: 0;"></path>
                                            </svg>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="content-page">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 mb-3">
                        <div class="d-flex align-items-center justify-content-between welcome-content">
                            <div class="navbar-breadcrumb">
                                <h4 class="mb-0 font-weight-700">Bienvenido a Talencan</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-3">
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="card card-block card-stretch card-height">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="mm-cart-image text-danger">
                                                <svg class="svg-icon svg-danger" width="50" height="52" id="h-01" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" style="stroke-dasharray: 61, 81; stroke-dashoffset: 0;"></path>
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M20.488 10H14V3.512A9.025 9.025 0 0120.488 9z" style="stroke-dasharray: 23, 43; stroke-dashoffset: 0;"></path>
                                                </svg>
                                            </div>
                                            <div class="mm-cart-text text-right">
                                                <h2 class="font-weight-700"><?php echo $usuariosTotales; ?></h2>
                                                <p class="mb-0 text-danger font-weight-600">Usuarios Totales</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="card card-block card-stretch card-height">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="mm-cart-image text-success">
                                                <svg class="svg-icon svg-success mr-4" width="50" height="52" id="h-02" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9 19h3v-6.058L8 9.454l-4 3.488V19h3v-4h2v4zm12 2H3a1 1 0 0 1-1-1v-7.513a1 1 0 0 1 .343-.754L6 8.544V4a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v16a1 1 0 0 1-1 1zm-5-10v2h2v-2h-2zm0 4v2h2v-2h-2zm0-8v2h2V7h-2zm-4 0v2h2V7h-2z" />
                                                </svg>
                                            </div>
                                            <div class="mm-cart-text text-right">
                                                <h2 class="font-weight-700"><?php echo $empresasTotales; ?></h2>
                                                <p class="mb-0 text-success font-weight-600">Empresas</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="card card-block card-stretch card-height">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="mm-cart-image text-primary">
                                                <svg class="svg-icon svg-blue mr-4" id="h-03" width="50" height="52" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.33 15.929A13.064 13.064 0 0 1 5 13c0-5.088 2.903-9.436 7-11.182C16.097 3.564 19 7.912 19 13c0 1.01-.114 1.991-.33 2.929l2.02 1.796a.5.5 0 0 1 .097.63l-2.458 4.096a.5.5 0 0 1-.782.096l-2.254-2.254a1 1 0 0 0-.707-.293H9.414a1 1 0 0 0-.707.293l-2.254 2.254a.5.5 0 0 1-.782-.096l-2.458-4.095a.5.5 0 0 1 .097-.631l2.02-1.796zM12 13a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" />

                                                </svg>
                                            </div>
                                            <div class="mm-cart-text text-right">
                                                <h2 class="font-weight-700"><?php echo $talentosTotales; ?></h2>
                                                <p class="mb-0 text-primary font-weight-600">Talento</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="card card-block card-stretch card-height">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="mm-cart-image text-warning">
                                                <svg class="svg-icon svg-warning mr-4" id="h-04" width="50" height="52" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 12l3-3 3 3 4-4M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" style="stroke-dasharray: 101, 121; stroke-dashoffset: 0;">
                                                    </path>
                                                </svg>
                                            </div>
                                            <div class="mm-cart-text text-right">
                                                <h2 class="font-weight-700"><?php echo $ofertasTotales; ?></h2>
                                                <p class="mb-0 text-warning font-weight-600">Ofertas</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>