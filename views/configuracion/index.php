<?php
include './views/header.php';
$user = $this->d['user'];
$talento = $this->d['talento'];
$empresa = $this->d['empresa'];
$aptitudesTodas = $this->d['aptitudesTodas'];
$aptitudes = $this->d['aptitudes'];
$aptitudTalento = $this->d['aptitudTalento'];

if ($talento->getTalentoId()) {
    $usuario = $talento;
}
if ($empresa->getEmpresaId()) {
    $usuario = $empresa;
}
?>
<div class="col-lg-12 mb-3">
    <div class="row">
        <div class="col-lg-12">
            <form method="post" action="<?php echo constant('URL'); ?>/configuracion/guardar">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input class='form-control' type='text' id='nombre' name='nombre' value='<?php echo $usuario->getNombre(); ?>' required>
                        </div>
                    </div>
                    <?php if ($talento->getTalentoId()) {; ?>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input class='form-control' type="text" id="apellidos" name="apellidos" value='<?php echo $usuario->getApellidos(); ?>' required>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input class='form-control' type="mail" name="email" id="email" value='<?php echo $usuario->getEmail(); ?>' required readonly>
                        </div>
                    </div>
                    <?php if ($talento->getTalentoId()) {; ?>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Nombre de los Estudios</label>
                                <input class='form-control' type="text" name="estudios" id="estudios" value='<?php echo $usuario->getEstudiosNombre(); ?>'>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Cualificación de los Estudios</label>
                                <input class='form-control' type="text" name="cualificacion" id="cualificacion" value='<?php echo $usuario->getEstudiosTipo(); ?>'>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Media de los Estudios</label>
                                <input class='form-control' type="text" name="media" id="media" value='<?php echo $usuario->getEstudiosMedia(); ?>'>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label>Aptitudes</label>
                            <div class="form-check aptitudes-grid">
                                <?php
                                foreach ($aptitudesTodas as $aptitud) {
                                    $aptitudNombre = array_values($aptitud)[0];
                                    $aptitudId = key($aptitud);
                                    echo "
                                    <label class='custom-control custom-checkbox custom-checkbox-color-check custom-control-inline'>
                                        <input class='form-check-input' type='checkbox' name='habilidades[]' value='$aptitudNombre'  ";
                                    foreach ($aptitudes as $aptitudNueva) {
                                        if (in_array($aptitudNombre, $aptitudNueva)) {
                                            echo "checked";
                                        }
                                    }
                                    echo ">
                                <div class='option_inner'>
                                    <div class='tickmark'></div>
                                    <div class='name'>$aptitudNombre</div>
                                </div>
                            </label>";
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($empresa->getempresaId()) { ?>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Sector</label>
                                <input class='form-control' type="text" name="sector" id="sector" value='<?php echo $usuario->getSector(); ?>'>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Localización</label>
                                <input class='form-control' type="text" name="localizacion" id="localizacion" value='<?php echo $usuario->getLocalizacion(); ?>'>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Web</label>
                                <input class='form-control' type="text" name="web" id="web" value='<?php echo $usuario->getWeb(); ?>'>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-lg-10">
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <input class="btn btn-primary" type="submit" name="guardar" value="Guardar">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
include './views/footer.php';
?>