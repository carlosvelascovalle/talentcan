<?php

class Controller
{

    function __construct()
    {
        $this->view = new View();
    }

    function loadModel($model)
    {
        error_log('CONTROLLER::loadModel()');
        $url = 'models/' . $model . 'model.php';

        if (file_exists($url)) {
            require_once $url;

            $modelName = $model . 'Model';
            $this->model = new $modelName();
        }
    }

    function existPOST($params)
    {
        error_log('CONTROLLER::existsPost()');
        foreach ($params as $param) {
            if (!isset($_POST[$param])) {
                error_log('CONTROLLER::existsPost -> No existe el parametro ' . $param);
                return false;
            }
        }
        error_log('CONTROLLER::existsPost -> Existen los parametros');
        return true;
    }

    function existGET($params)
    {
        error_log('CONTROLLER::existGET()');
        foreach ($params as $param) {
            if (!isset($_GET[$param])) {
                error_log('CONTROLLER::existsGet -> No existe el parametro ' . $param);
                return false;
            }
        }
        error_log('CONTROLLER::existsGet -> Existen los parametros');
        return true;
    }

    function getPOST($name)
    {
        return $_POST[$name];
    }

    function getGET($name)
    {
        return $_GET[$name];
    }

    function redirect($route, $messages=[])
    {
        error_log('CONTROLLER::redirect -> '.$route);
        ob_start();
        $data = [];
        $params = '';

        foreach ($messages as $key => $message) {
            array_push($data, $key . '=' . $message);
        }

        $params = join('&', $data);

        if ($params != '') {
            $params = '?' . $params;
        }
        error_log('CONTROLLER::redirect -> '.'Location: ' . constant('URL') .'/'. $route . $params);
        header('Location: ' . constant('URL') .'/'. $route . $params);
        ob_end_flush();
    }
}
?>