<?php
class AptitudModel extends Model implements IModel
{

    private $aptitud_id;
    private $nombre;

    public function __construct()
    {
        parent::__construct();
        $this->nombre = '';
    }

    public function save()
    {
        try {
            error_log($this->nombre);
            $query = $this->prepare('INSERT INTO aptitud(nombre) VALUES (:nombre)');
            $query->execute([
                'nombre' => $this->nombre,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::save->PDOException ' . $e);
            return false;
        }
    }

    public function getAll()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM aptitud');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new  AptitudModel();
                $item->setId($p['aptitud_id']);
                $item->setNombre($p['nombre']);

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function get($aptitud_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM aptitud WHERE aptitud_id = :aptitud_id');
            $query->execute(['aptitud_id' => $aptitud_id]);

            $aptitud = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('USERMODEL::getId->PDOException ' . $aptitud['nombre']);
            if($aptitud){
            $this->aptitud_id = $aptitud['aptitud_id'];
            $this->nombre = $aptitud['nombre'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function getByNombre($nombre)
    {
        try {
            $query = $this->prepare('SELECT * FROM aptitud WHERE nombre = :nombre');
            $query->execute(['nombre' => $nombre]);

            $aptitud = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('USERMODEL::getId->PDOException ' . $aptitud['nombre']);
            if($aptitud){
            $this->aptitud_id = $aptitud['aptitud_id'];
            $this->nombre = $aptitud['nombre'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function delete($aptitud_id)
    {
        try {
            $query = $this->prepare('DELETE FROM aptitud WHERE aptitud_id = :aptitud_id');
            $query->execute([
                'aptitud_id' => $aptitud_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::delete->PDOException ' . $e);
            return false;
        }
    }

    public function update()
    {
        try {
            $query = $this->prepare('UPDATE SET nombre = :nombre WHERE aptitud_id = :aptitud_id');
            $query->execute([
                'aptitud_id' => $this->aptitud_id,
                'nombre' => $this->nombre,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::update->PDOException ' . $e);
            return false;
        }
    }

    // Pasar un arreglo para añadir un miembro
    public function from($array)
    {
        $this->aptitud_id = $array['nombre'];
        $this->nombre = $array['nombre'];
    }

    public function exists($nombre)
    {
        try {
            $query = $this->prepare('SELECT nombre FROM aptitud WHERE nombre = :nombre');
            $query->execute(['nombre' => $nombre]);

            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            error_log('USERMODEL::exists->PDOException ' . $e);
            return false;
        }
    }

    public function getId()
    {
        return $this->aptitud_id;
    }

    public function setId($aptitud_id)
    {
        $this->aptitud_id = $aptitud_id;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

}
