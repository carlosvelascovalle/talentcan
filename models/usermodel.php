<?php
class UserModel extends Model implements IModel
{

    private $id;
    private $username;
    private $password;
    private $rol;

    public function __construct()
    {
        parent::__construct();
        $this->username = '';
        $this->password = '';
        $this->rol = '';
    }

    public function save()
    {
        try {
            error_log($this->username);
            $query = $this->prepare('INSERT INTO user(username, password, rol) VALUES (:username, :password, :rol)');
            $query->execute([
                'username' => $this->username,
                'password' => $this->password,
                'rol' => $this->rol,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::save->PDOException ' . $e);
            return false;
        }
    }

    public function getAll()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM user');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new UserModel();
                $item->setId($p['id']);
                $item->setUsername($p['username']);
                $item->setPassword($p['password']);
                $item->setRole($p['rol']);

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function get($id)
    {
        try {
            $query = $this->prepare('SELECT * FROM user WHERE id = :id');
            $query->execute(['username' => $id]);

            $user = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('USERMODEL::getId->PDOException ' . $user['username']);
            if($user){
            $this->id = $user['id'];
            $this->username = $user['username'];
            $this->password = $user['password'];
            $this->rol = $user['rol'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function getByUsername($username)
    {
        try {
            $query = $this->prepare('SELECT * FROM user WHERE username = :username');
            $query->execute(['username' => $username]);

            $user = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('USERMODEL::getId->PDOException ' . $user['username']);
            if($user){
            $this->id = $user['id'];
            $this->username = $user['username'];
            $this->password = $user['password'];
            $this->rol = $user['rol'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $query = $this->prepare('DELETE FROM user WHERE id = :id');
            $query->execute([
                'id' => $id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::delete->PDOException ' . $e);
            return false;
        }
    }

    public function update()
    {
        try {
            $query = $this->prepare('UPDATE SET username = :username, password = :password, rol = :rol WHERE id = :id');
            $query->execute([
                'id' => $this->id,
                'username' => $this->username,
                'password' => $this->password,
                'rol' => $this->rol,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::update->PDOException ' . $e);
            return false;
        }
    }

    // Pasar un arreglo para añadir un miembro
    public function from($array)
    {
        $this->id = $array['username'];
        $this->username = $array['username'];
        $this->password = $array['password'];
        $this->rol = $array['rol'];
    }

    public function exists($username)
    {
        try {
            $query = $this->prepare('SELECT username FROM user WHERE username = :username');
            $query->execute(['username' => $username]);

            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            error_log('USERMODEL::exists->PDOException ' . $e);
            return false;
        }
    }

    public function comparePasswords($password, $id)
    {
        try {
            $user = $this->get($id);

            return password_verify($password, $user->getPassword());
        } catch (PDOException $e) {
            error_log('USERMODEL::comparePasswords->PDOException ' . $e);
            return false;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $this->getHashedPassword($password);


        return $this;
    }

    public function getRole()
    {
        return $this->rol;
    }

    public function setRole($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    private function getHashedPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 10]);
    }
}
