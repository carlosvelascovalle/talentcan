<?php
class TalentoModel extends Model implements IModel
{
    private $talento_id;
    private $nombre;
    private $apellidos;
    private $email;
    private $estudios_nombre;
    private $estudios_tipo;
    private $estudios_media;
    private $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->nombre = '';
        $this->apellidos = '';
        $this->email = '';
        $this->estudios_nombre = '';
        $this->estudios_tipo = '';
        $this->estudios_media = '';
        $this->user_id = '';
    }

    public function save()
    {
        try {
            error_log($this->nombre);
            $query = $this->prepare('INSERT INTO talento(
                nombre,
                apellidos,
                email,
                estudios_nombre,
                estudios_tipo,
                estudios_media,
                user_id) VALUES (
                    :nombre,
                    :apellidos,
                    :email,
                    :estudios_nombre,
                    :estudios_tipo,
                    :estudios_media,
                    :user_id)');
            $query->execute([
                'nombre' => $this->nombre,
                'apellidos' => $this->apellidos,
                'email' => $this->email,
                'estudios_nombre' => $this->estudios_nombre,
                'estudios_tipo' => $this->estudios_tipo,
                'estudios_media' => $this->estudios_media,
                'user_id' => $this->user_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('TALENTOMODEL::save->PDOException ' . $e);
            return false;
        }
    }

    public function getAll()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM talento');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new TalentoModel();
                $item->setTalentoId($p['talento_id']);
                $item->setNombre($p['nombre']);
                $item->setApellidos($p['apellidos']);
                $item->setEmail($p['email']);
                $item->setEstudiosNombre($p['estudios_nombre']);
                $item->setEstudiosTipo($p['estudios_tipo']);
                $item->setEstudiosMedia($p['estudios_media']);
                $item->setUserId($p['user_id']);
                
                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('TALENTOMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function get($user_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM talento WHERE user_id = :user_id');
            $query->execute(['user_id' => $user_id]);

            $talento = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('TALENTOMODEL::getId->PDOException ' . $talento['talento_id']);
            if ($talento) {
                $this->talento_id = $talento['talento_id'];
                $this->nombre = $talento['nombre'];
                $this->apellidos = $talento['apellidos'];
                $this->email = $talento['email'];
                $this->estudios_nombre = $talento['estudios_nombre'];
                $this->estudios_tipo = $talento['estudios_tipo'];
                $this->estudios_media = $talento['estudios_media'];
                $this->user_id = $talento['user_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('TALENTOMODEL::getId->PDOException ' . $e);
            return false;
        }
    }
    public function getByTalentoId($id)
    {
        try {
            $query = $this->prepare('SELECT * FROM talento WHERE talento_id = :id');
            $query->execute(['id' => $id]);

            $talento = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('EMPRESAMODEL::getId->PDOException ' . $empresa['nombre']);
            if ($talento) {
                $this->talento_id = $talento['talento_id'];
                $this->nombre = $talento['nombre'];
                $this->apellidos = $talento['apellidos'];
                $this->email = $talento['email'];
                $this->estudios_nombre = $talento['estudios_nombre'];
                $this->estudios_tipo = $talento['estudios_tipo'];
                $this->estudios_media = $talento['estudios_media'];
                $this->user_id = $talento['user_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('TALENTOMODEL::getId->PDOException ' . $e);
            return false;
        }
    }
    public function getByNombre($nombre)
    {
        try {
            $query = $this->prepare('SELECT * FROM talento WHERE nombre = :nombre');
            $query->execute(['nombre' => $nombre]);

            $talento = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('TALENTOMODEL::getId->PDOException ' . $talento['nombre']);
            if ($talento) {
                $this->talento_id = $talento['talento_id'];
                $this->nombre =  $talento['nombre'];
                $this->apellidos = $talento['apellidos'];
                $this->email = $talento['email'];
                $this->estudios_nombre = $talento['estudios_nombre'];
                $this->estudios_tipo = $talento['estudios_tipo'];
                $this->estudios_media = $talento['estudios_media'];
                $this->user_id = $talento['user_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('TALENTOMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function delete($talento_id)
    {
        try {
            $query = $this->prepare('DELETE FROM talento WHERE talento_id = :talento_id');
            $query->execute([
                'talento_id' => $talento_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('TALENTOMODEL::delete->PDOException ' . $e);
            return false;
        }
    }

    public function update()
    {
        try {
            $query = $this->prepare('UPDATE talento SET nombre = :nombre, apellidos = :apellidos, email = :email , estudios_nombre = :estudios_nombre , estudios_tipo = :estudios_tipo , estudios_media = :estudios_media WHERE user_id = :user_id');
            $query->execute([
                'user_id' => $this->user_id,
                'nombre' => $this->nombre,
                'apellidos' => $this->apellidos,
                'email' => $this->email,
                'estudios_nombre' => $this->estudios_nombre,
                'estudios_tipo' => $this->estudios_tipo,
                'estudios_media' => $this->estudios_media,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('TALENTOMODEL::update->PDOException ' . $e);
            return false;
        }
    }

    // Pasar un arreglo para añadir un miembro
    public function from($array)
    {      
        $this->talento_id = $array['talento_id'];
        $this->nombre = $array['nombre'];
        $this->apellidos = $array['apellidos'];
        $this->email = $array['email'];
        $this->estudios_nombre = $array['estudios_nombre'];
        $this->estudios_tipo = $array['estudios_tipo'];
        $this->estudios_media = $array['estudios_media'];
        $this->user_id = $array['user_id'];
    }

    public function exists($nombre)
    {
        try {
            $query = $this->prepare('SELECT nombre FROM talento WHERE nombre = :nombre');
            $query->execute(['nombre' => $nombre]);

            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            error_log('TALENTOMODEL::exists->PDOException ' . $e);
            return false;
        }
    }

    public function getTalentoId()
    {
        return $this->talento_id;
    }

    public function setTalentoId($talento_id)
    {
        $this->talento_id = $talento_id;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellidos()
    {
        return $this->apellidos;
    }

    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;


        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getEstudiosNombre()
    {
        return $this->estudios_nombre;
    }

    public function setEstudiosNombre($estudios_nombre)
    {
        $this->estudios_nombre = $estudios_nombre;

        return $this;
    }
    public function getEstudiosTipo()
    {
        return $this->estudios_tipo;
    }

    public function setEstudiosTipo($estudios_tipo)
    {
        $this->estudios_tipo = $estudios_tipo;

        return $this;
    }
    public function getEstudiosMedia()
    {
        return $this->estudios_media;
    }

    public function setEstudiosMedia($estudios_media)
    {
        $this->estudios_media = $estudios_media;

        return $this;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }
}
