<?php
class AptitudTalentoModel extends Model implements IModel
{

    private $aptitud_talento_id;
    private $aptitud_id;
    private $talento_id;

    public function __construct()
    {
        parent::__construct();
        $this->aptitud_id = '';
        $this->talento_id = '';
    }

    public function save()
    {
        try {
            error_log($this->talento_id);
            $query = $this->prepare('INSERT INTO aptitud_talento(aptitud_id, talento_id) VALUES (:aptitud_id, :talento_id)');
            $query->execute([
                'aptitud_id' => $this->aptitud_id,
                'talento_id' => $this->talento_id,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::save->PDOException ' . $e);
            return false;
        }
    }

    public function getAll()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM aptitud_talento');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new  AptitudTalentoModel();
                $item->setId($p['aptitud_talento_id']);
                $item->setAptitudId($p['aptitud_id']);
                $item->setTalentoId($p['talento_id']);
                
                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function get($aptitud_talento_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM aptitud_talento WHERE aptitud_talento_id = :aptitud_talento_id');
            $query->execute(['aptitud_talento_id' => $aptitud_talento_id]);

            $aptitud_talento = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('USERMODEL::getId->PDOException ' . $aptitud_talento['talento_id']);
            if($aptitud_talento){
            $this->aptitud_talento_id = $aptitud_talento['aptitud_talento_id'];
            $this->aptitud_id = $aptitud_talento['aptitud_id'];
            $this->talento_id = $aptitud_talento['talento_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function getByTalento($talento_id)
    {
        $items=[];
        try {
            $query = $this->prepare('SELECT * FROM aptitud_talento WHERE talento_id = :talento_id');
            $query->execute(['talento_id' => $talento_id]);

            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new  AptitudTalentoModel();
                $item->setId($p['aptitud_talento_id']);
                $item->setAptitudId($p['aptitud_id']);
                $item->setTalentoId($p['talento_id']);
                
                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }
    public function getByAptitud($aptitud_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM aptitud_talento WHERE aptitud_id = :aptitud_id');
            $query->execute(['aptitud_id' => $aptitud_id]);

            $aptitud_talento = $query->fetch(PDO::FETCH_ASSOC);
            if($aptitud_talento){
            $this->aptitud_id = $aptitud_talento['aptitud_id'];
            $this->talento_id = $aptitud_talento['talento_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function delete($aptitud_talento_id)
    {
        try {
            $query = $this->prepare('DELETE FROM aptitud_talento WHERE aptitud_talento_id = :aptitud_talento_id');
            $query->execute([
                'aptitud_talento_id' => $aptitud_talento_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::delete->PDOException ' . $e);
            return false;
        }
    }

    public function update()
    {
        try {
            $query = $this->prepare('UPDATE SET talento_id = :talento_id, aptitud_id = :aptitud_id WHERE aptitud_talento_id = :aptitud_talento_id');
            $query->execute([
                'aptitud_talento_id' => $this->aptitud_talento_id,
                'aptitud_id' => $this->aptitud_id,
                'talento_id' => $this->talento_id,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::update->PDOException ' . $e);
            return false;
        }
    }

    // Pasar un arreglo para añadir un miembro
    public function from($array)
    {
        $this->aptitud_talento_id = $array['talento_id'];
        $this->aptitud_id = $array['aptitud_id'];
        $this->talento_id = $array['talento_id'];
    }

    public function exists($talento_id)
    {
        try {
            $query = $this->prepare('SELECT talento_id FROM aptitud_talento WHERE talento_id = :talento_id');
            $query->execute(['talento_id' => $talento_id]);

            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            error_log('USERMODEL::exists->PDOException ' . $e);
            return false;
        }
    }

    public function getId()
    {
        return $this->aptitud_talento_id;
    }

    public function setId($aptitud_talento_id)
    {
        $this->aptitud_talento_id = $aptitud_talento_id;

        return $this;
    }

    public function getAptitudId()
    {
        return $this->aptitud_id;
    }

    public function setAptitudId($aptitud_id)
    {
        $this->aptitud_id = $aptitud_id;

        return $this;
    }

    public function getTalentoId()
    {
        return $this->talento_id;
    }

    public function setTalentoId($talento_id)
    {
        $this->talento_id = $talento_id;

        return $this;
    }

}
