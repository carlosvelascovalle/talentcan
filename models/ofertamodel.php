<?php
class OfertaModel extends Model implements IModel
{
    private $oferta_id;
    private $nombre;
    private $jornada;
    private $estudio_minimo;
    private $descripcion;
    private $salario;
    private $empresa_id;

    public function __construct()
    {
        parent::__construct();
        $this->nombre = '';
        $this->jornada = '';
        $this->estudio_minimo = '';
        $this->descripcion = '';
        $this->salario = '';
        $this->empresa_id = '';
    }
    public function save()
    {
        try {
            error_log($this->nombre);
            $query = $this->prepare('INSERT INTO oferta(
                nombre,
                jornada,
                estudio_minimo,
                descripcion,
                salario,
                empresa_id) VALUES (
                    :nombre,
                    :jornada,
                    :estudio_minimo,
                    :descripcion,
                    :salario,
                    :empresa_id)');
            $query->execute([
                'nombre' => $this->nombre,
                'jornada' => $this->jornada,
                'estudio_minimo' => $this->estudio_minimo,
                'descripcion' => $this->descripcion,
                'salario' => $this->salario,
                'empresa_id' => $this->empresa_id
            ]);
            return true;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::save->PDOException ' . $e);
            return false;
        }
    }

    public function getAll()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM oferta');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new OfertaModel();
                $item->setOfertaId($p['oferta_id']);
                $item->setNombre($p['nombre']);
                $item->setJornada($p['jornada']);
                $item->setEstudioMinimo($p['estudio_minimo']);
                $item->setDescripcion($p['descripcion']);
                $item->setSalario($p['salario']);
                $item->setEmpresaId($p['empresa_id']);

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function getAllOrderBySalario()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM oferta ORDER BY `salario` DESC');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new OfertaModel();
                $item->setOfertaId($p['oferta_id']);
                $item->setNombre($p['nombre']);
                $item->setJornada($p['jornada']);
                $item->setEstudioMinimo($p['estudio_minimo']);
                $item->setDescripcion($p['descripcion']);
                $item->setSalario($p['salario']);
                $item->setEmpresaId($p['empresa_id']);

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function get($oferta_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM oferta WHERE oferta_id = :oferta_id');
            $query->execute(['oferta_id' => $oferta_id]);

            $oferta = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('EMPRESAMODEL::getId->PDOException ' . $oferta['oferta_id']);
            if ($oferta) {
                $this->oferta_id = $oferta['oferta_id'];
                $this->nombre = $oferta['nombre'];
                $this->jornada = $oferta['jornada'];
                $this->estudio_minimo = $oferta['estudio_minimo'];
                $this->descripcion = $oferta['descripcion'];
                $this->salario = $oferta['salario'];
                $this->empresa_id = $oferta['empresa_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function getByNombre($nombre)
    {
        try {
            $query = $this->prepare('SELECT * FROM oferta WHERE nombre = :nombre');
            $query->execute(['nombre' => $nombre]);

            $oferta = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('EMPRESAMODEL::getId->PDOException ' . $oferta['nombre']);
            if ($oferta) {
                $this->oferta_id = $oferta['oferta_id'];
                $this->nombre =  $oferta['nombre'];
                $this->jornada = $oferta['jornada'];
                $this->estudio_minimo = $oferta['estudio_minimo'];
                $this->descripcion = $oferta['descripcion'];
                $this->salario = $oferta['salario'];
                $this->empresa_id = $oferta['empresa_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function delete($oferta_id)
    {
        try {
            $query = $this->prepare('DELETE FROM oferta WHERE oferta_id = :oferta_id');
            $query->execute([
                'oferta_id' => $oferta_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::delete->PDOException ' . $e);
            return false;
        }
    }

    public function update()
    {
        try {
            $query = $this->prepare('UPDATE SET nombre = :nombre,
            jornada = :jornada,
            estudio_minimo = :estudio_minimo,
            descripcion = :descripcion,
            salario = :salario,
            WHERE oferta_id = :oferta_id');
            $query->execute([
                'oferta_id' => $this->oferta_id,
                'nombre' => $this->nombre,
                'jornada' => $this->jornada,
                'estudio_minimo' => $this->estudio_minimo,
                'descripcion' => $this->descripcion,
                'salario' => $this->salario,
                'empresa_id' => $this->empresa_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::update->PDOException ' . $e);
            return false;
        }
    }

    // Pasar un arreglo para añadir un miembro
    public function from($array)
    {
        $this->oferta_id = $array['oferta_id'];
        $this->nombre =  $array['nombre'];
        $this->jornada = $array['jornada'];
        $this->estudio_minimo = $array['estudio_minimo'];
        $this->descripcion = $array['descripcion'];
        $this->salario = $array['salario'];
        $this->empresa_id = $array['empresa_id'];
    }

    public function exists($nombre)
    {
        try {
            $query = $this->prepare('SELECT nombre FROM oferta WHERE nombre = :nombre');
            $query->execute(['nombre' => $nombre]);

            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::exists->PDOException ' . $e);
            return false;
        }
    }

    public function buscarOferta($busqueda)
    {
        $items = [];
        try {
            $query = $this->query("SELECT * FROM ofertas WHERE nombre LIKE '%" . $busqueda . "%' OR jornada LIKE '%" . $busqueda . "%' OR estudio_minimo LIKE '%" . $busqueda . "%' OR descripcion LIKE '%" . $busqueda . "%' OR salario LIKE '%" . $busqueda . "%'");
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new OfertaModel();
                $item->setOfertaId($p['oferta_id']);
                $item->setNombre($p['nombre']);
                $item->setJornada($p['jornada']);
                $item->setEstudioMinimo($p['estudio_minimo']);
                $item->setDescripcion($p['descripcion']);
                $item->setSalario($p['salario']);
                $item->setEmpresaId($p['empresa_id']);
                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::exists->PDOException ' . $e);
            return false;
        }
    }

    public function getOfertaId()
    {
        return $this->ofertaId;
    }

    public function setOfertaId($ofertaId)
    {
        $this->ofertaId = $ofertaId;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getJornada()
    {
        return $this->jornada;
    }

    public function setJornada($jornada)
    {
        $this->jornada = $jornada;


        return $this;
    }

    public function getEstudioMinimo()
    {
        return $this->estudio_minimo;
    }

    public function setEstudioMinimo($estudio_minimo)
    {
        $this->estudio_minimo = $estudio_minimo;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getSalario()
    {
        return $this->salario;
    }

    public function setSalario($salario)
    {
        $this->salario = $salario;

        return $this;
    }

    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    public function setEmpresaId($empresa_id)
    {
        $this->empresa_id = $empresa_id;

        return $this;
    }
}
