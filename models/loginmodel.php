<?php

class LoginModel extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    function login($username,$password){
        try {
            $query = $this->prepare('SELECT * FROM user WHERE username = :username');
            $query->execute(['username'=>$username]);

            if($query->rowCount() == 1){
                $item = $query->fetch(PDO::FETCH_ASSOC);

                $user = new UserModel();
                $user->from($item);

                error_log('LOGINMODEL::Login: user id '.$user->getId());

                if(password_verify($password,$user->getPassword())){
                    error_log('LOGINMODEL::login->Success');
                    return $user;
                }else{
                    error_log('LOGINMODEL::login->Password Erroneo');
                    return NULL;
                }
            }

        } catch (PDOException $e) {
            error_log('LOGINMODEL::login->Excepcion'. $e);
            return NULL;
        }
    }
}
