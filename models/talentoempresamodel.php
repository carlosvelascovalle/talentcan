<?php
class TalentoEmpresaModel extends Model implements IModel
{

    private $talento_empresa_id;
    private $talento_id;
    private $empresa_id;

    public function __construct()
    {
        parent::__construct();
        $this->talento_id = '';
        $this->empresa_id = '';
    }

    public function save()
    {
        try {
            $query = $this->prepare('INSERT INTO talento_empresa(talento_id, empresa_id) VALUES (:talento_id, :empresa_id)');
            $query->execute([
                'talento_id' => $this->talento_id,
                'empresa_id' => $this->empresa_id,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::save->PDOException ' . $e);
            return false;
        }
    }

    public function getAll()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM talento_empresa');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new  TalentoEmpresaModel();
                $item->setId($p['talento_empresa_id']);
                $item->setEmpresaId($p['talento_id']);
                $item->setEmpresaId($p['empresa_id']);

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function get($talento_empresa_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM talento_empresa WHERE talento_empresa_id = :talento_empresa_id');
            $query->execute(['talento_empresa_id' => $talento_empresa_id]);

            $talento_empresa = $query->fetch(PDO::FETCH_ASSOC);
            if ($talento_empresa) {
                $this->talento_empresa_id = $talento_empresa['talento_empresa_id'];
                $this->talento_id = $talento_empresa['talento_id'];
                $this->empresa_id = $talento_empresa['empresa_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function getByEmpresa($empresa_id)
    {
        $items = [];
        try {
            $query = $this->prepare('SELECT * FROM talento_empresa WHERE empresa_id = :empresa_id');
            $query->execute(['empresa_id' => $empresa_id]);

            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new  TalentoEmpresaModel();
                $item->setId($p['talento_empresa_id']);
                $item->setTalentoId($p['talento_id']);
                $item->setEmpresaId($p['empresa_id']);

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }
    public function getByTalento($talento_id)
    {
        $items =[];
        try {
            $query = $this->prepare('SELECT * FROM talento_empresa WHERE talento_id=:talento_id');
            $query->execute(['talento_id' => $talento_id]);

            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new  TalentoEmpresaModel();
                $item->setId($p['talento_empresa_id']);
                $item->setTalentoId($p['talento_id']);
                $item->setEmpresaId($p['empresa_id']);

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function getByTalentoEmpresa($talento_id,$empresa_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM talento_empresa WHERE talento_id=:talento_id AND empresa_id=:empresa_id');
            $query->execute([
                'empresa_id' => $empresa_id,
                'talento_id' => $talento_id
            ]);

            $talento_empresa = $query->fetch(PDO::FETCH_ASSOC);
            if ($talento_empresa) {
                $this->talento_empresa_id = $talento_empresa['talento_empresa_id'];
                $this->talento_id = $talento_empresa['talento_id'];
                $this->empresa_id = $talento_empresa['empresa_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function delete($talento_empresa_id)
    {
        try {
            $query = $this->prepare('DELETE FROM talento_empresa WHERE talento_empresa_id = :talento_empresa_id');
            $query->execute([
                'talento_empresa_id' => $talento_empresa_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::delete->PDOException ' . $e);
            return false;
        }
    }

    public function update()
    {
        try {
            $query = $this->prepare('UPDATE SET empresa_id = :empresa_id, talento_id = :talento_id WHERE talento_empresa_id = :talento_empresa_id');
            $query->execute([
                'talento_empresa_id' => $this->talento_empresa_id,
                'talento_id' => $this->talento_id,
                'empresa_id' => $this->empresa_id,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::update->PDOException ' . $e);
            return false;
        }
    }

    // Pasar un arreglo para añadir un miembro
    public function from($array)
    {
        $this->talento_empresa_id = $array['empresa_id'];
        $this->talento_id = $array['talento_id'];
        $this->empresa_id = $array['empresa_id'];
    }

    public function exists($talento_id, $empresa_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM talento_empresa WHERE empresa_id=:empresa_id AND talento_id=:talento_id');
            $query->execute([
                'empresa_id' => $empresa_id,
                'talento_id' => $talento_id
            ]);
            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            error_log('USERMODEL::exists->PDOException ' . $e);
            return false;
        }
    }

    public function getId()
    {
        return $this->talento_empresa_id;
    }

    public function setId($talento_empresa_id)
    {
        $this->talento_empresa_id = $talento_empresa_id;

        return $this;
    }

    public function getTalentoId()
    {
        return $this->talento_id;
    }

    public function setTalentoId($talento_id)
    {
        $this->talento_id = $talento_id;

        return $this;
    }

    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    public function setEmpresaId($empresa_id)
    {
        $this->empresa_id = $empresa_id;

        return $this;
    }
}
