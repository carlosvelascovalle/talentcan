<?php
class EmpresaModel extends Model implements IModel
{
    private $empresa_id;
    private $nombre;
    private $apellidos;
    private $email;
    private $sector;
    private $localizacion;
    private $web;
    private $user_id;

    public function __construct()
    {
        parent::__construct();
        $this->nombre = '';
        $this->apellidos = '';
        $this->email = '';
        $this->sector = '';
        $this->localizacion = '';
        $this->web = '';
        $this->user_id = '';
    }

    public function save()
    {
        try {
            error_log($this->nombre);
            $query = $this->prepare('INSERT INTO empresa(
                nombre,
                apellidos,
                email,
                sector,
                localizacion,
                web,
                user_id) VALUES (
                    :nombre,
                    :apellidos,
                    :email,
                    :sector,
                    :localizacion,
                    :web,
                    :user_id)');
            $query->execute([
                'nombre' => $this->nombre,
                'apellidos' => $this->apellidos,
                'email' => $this->email,
                'sector' => $this->sector,
                'localizacion' => $this->localizacion,
                'web' => $this->web,
                'user_id' => $this->user_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::save->PDOException ' . $e);
            return false;
        }
    }

    public function getAll()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM empresa');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new EmpresaModel();
                $item->setEmpresaId($p['empresa_id']);
                $item->setNombre($p['nombre']);
                $item->setApellidos($p['apellidos']);
                $item->setEmail($p['email']);
                $item->setSector($p['sector']);
                $item->setLocalizacion($p['localizacion']);
                $item->setWeb($p['web']);
                $item->setUserId($p['user_id']);

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function getAllByTalentoId()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM empresa');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new EmpresaModel();
                $item->setEmpresaId($p['empresa_id']);
                $item->setNombre($p['nombre']);
                $item->setApellidos($p['apellidos']);
                $item->setEmail($p['email']);
                $item->setSector($p['sector']);
                $item->setLocalizacion($p['localizacion']);
                $item->setWeb($p['web']);
                $item->setUserId($p['user_id']);

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function get($user_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM empresa WHERE user_id = :user_id');
            $query->execute(['user_id' => $user_id]);

            $empresa = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('EMPRESAMODEL::getId->PDOException ' . $empresa['empresa_id']);
            if ($empresa) {
                $this->empresa_id = $empresa['empresa_id'];
                $this->nombre = $empresa['nombre'];
                $this->apellidos = $empresa['apellidos'];
                $this->email = $empresa['email'];
                $this->sector = $empresa['sector'];
                $this->localizacion = $empresa['localizacion'];
                $this->web = $empresa['web'];
                $this->user_id = $empresa['user_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function getByEmpresaId($id)
    {
        try {
            $query = $this->prepare('SELECT * FROM empresa WHERE empresa_id = :id');
            $query->execute(['id' => $id]);

            $empresa = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('EMPRESAMODEL::getId->PDOException ' . $empresa['nombre']);
            if ($empresa) {
                $this->empresa_id = $empresa['empresa_id'];
                $this->nombre =  $empresa['nombre'];
                $this->apellidos = $empresa['apellidos'];
                $this->email =  $empresa['email'];
                $this->sector = $empresa['sector'];
                $this->localizacion = $empresa['localizacion'];
                $this->web = $empresa['web'];
                $this->user_id = $empresa['user_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::getId->PDOException ' . $e);
            return false;
        }
    }
    public function getByNombre($nombre)
    {
        try {
            $query = $this->prepare('SELECT * FROM empresa WHERE nombre = :nombre');
            $query->execute(['nombre' => $nombre]);

            $empresa = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('EMPRESAMODEL::getId->PDOException ' . $empresa['nombre']);
            if ($empresa) {
                $this->empresa_id = $empresa['empresa_id'];
                $this->nombre =  $empresa['nombre'];
                $this->apellidos = $empresa['apellidos'];
                $this->email =  $empresa['email'];
                $this->sector = $empresa['sector'];
                $this->localizacion = $empresa['localizacion'];
                $this->web = $empresa['web'];
                $this->user_id = $empresa['user_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function delete($empresa_id)
    {
        try {
            $query = $this->prepare('DELETE FROM empresa WHERE empresa_id = :empresa_id');
            $query->execute([
                'empresa_id' => $empresa_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::delete->PDOException ' . $e);
            return false;
        }
    }

    public function update()
    {
        try {
            $query = $this->prepare('UPDATE empresa SET nombre = :nombre, apellidos = :apellidos,
                email = :email, sector = :sector, localizacion = :localizacion, web = :web WHERE user_id = :user_id');
            $query->execute([
                'user_id' => $this->user_id,
                'nombre' => $this->nombre,
                'apellidos' => $this->apellidos,
                'email' => $this->email,
                'sector' => $this->sector,
                'localizacion' => $this->localizacion,
                'web' => $this->web,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::update->PDOException ' . $e);
            return false;
        }
    }

    // Pasar un arreglo para añadir un miembro
    public function from($array)
    {      
        $this->empresa_id = $array['empresa_id'];
        $this->nombre = $array['nombre'];
        $this->apellidos = $array['apellidos'];
        $this->email = $array['email'];
        $this->sector = $array['sector'];
        $this->localizacion = $array['localizacion'];
        $this->web = $array['web'];
        $this->user_id = $array['user_id'];
    }

    public function exists($nombre)
    {
        try {
            $query = $this->prepare('SELECT nombre FROM empresa WHERE nombre = :nombre');
            $query->execute(['nombre' => $nombre]);

            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            error_log('EMPRESAMODEL::exists->PDOException ' . $e);
            return false;
        }
    }

    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    public function setEmpresaId($empresa_id)
    {
        $this->empresa_id = $empresa_id;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getSector()
    {
        return $this->sector;
    }

    public function setSector($sector)
    {
        $this->sector = $sector;


        return $this;
    }

    public function getLocalizacion()
    {
        return $this->localizacion;
    }

    public function setLocalizacion($localizacion)
    {
        $this->localizacion = $localizacion;

        return $this;
    }

    public function getWeb()
    {
        return $this->web;
    }

    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    public function getApellidos()
    {
        return $this->apellidos;
    }

    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }
}
