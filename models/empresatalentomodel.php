<?php
class EmpresaTalentoModel extends Model implements IModel
{

    private $empresa_talento_id;
    private $talento_id;
    private $empresa_id;

    public function __construct()
    {
        parent::__construct();
        $this->talento_id = '';
        $this->empresa_id = '';
    }

    public function save()
    {
        try {
            $query = $this->prepare('INSERT INTO empresa_talento(talento_id, empresa_id) VALUES (:talento_id, :empresa_id)');
            $query->execute([
                'talento_id' => $this->talento_id,
                'empresa_id' => $this->empresa_id,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::save->PDOException ' . $e);
            return false;
        }
    }

    public function getAll()
    {
        $items = [];
        try {
            $query = $this->query('SELECT * FROM empresa_talento');
            // FETCH_ASSOC objeto clave->valor
            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new  TalentoEmpresaModel();
                $item->setId($p['empresa_talento_id']);
                $item->setEmpresaId($p['talento_id']);
                $item->setEmpresaId($p['empresa_id']);
                
                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getAll->PDOException ' . $e);
            return false;
        }
    }

    public function get($empresa_talento_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM empresa_talento WHERE empresa_talento_id = :empresa_talento_id');
            $query->execute(['empresa_talento_id' => $empresa_talento_id]);

            $empresa_talento = $query->fetch(PDO::FETCH_ASSOC);
            //error_log('USERMODEL::getId->PDOException ' . $empresa_talento['empresa_id']);
            if($empresa_talento){
            $this->empresa_talento_id = $empresa_talento['empresa_talento_id'];
            $this->talento_id = $empresa_talento['talento_id'];
            $this->empresa_id = $empresa_talento['empresa_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function getByEmpresa($empresa_id)
    {
        $items=[];
        try {
            $query = $this->prepare('SELECT * FROM empresa_talento WHERE empresa_id = :empresa_id');
            $query->execute(['empresa_id' => $empresa_id]);

            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new EmpresatalentoModel();
                $item->setId($p['empresa_talento_id']);
                $item->setTalentoId($p['talento_id']);
                $item->setEmpresaId($p['empresa_id']);
                
                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }
    public function getBytalento($talento_id)
    {
        $items =[];
        try {
            $query = $this->prepare('SELECT * FROM empresa_talento WHERE talento_id = :talento_id');
            $query->execute(['talento_id' => $talento_id]);

            while ($p = $query->fetch(PDO::FETCH_ASSOC)) {
                $item = new  TalentoEmpresaModel();
                $item->setId($p['empresa_talento_id']);
                $item->setEmpresaId($p['talento_id']);
                $item->setEmpresaId($p['empresa_id']);
                
                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function getByEmpresaTalento($talento_id,$empresa_id)
    {
        try {
            $query = $this->prepare('SELECT * FROM empresa_talento WHERE talento_id = :talento_id AND empresa_id = :empresa_id');
            $query->execute([
                'empresa_id' => $empresa_id,
                'talento_id' => $talento_id
            ]);

            $empresa_talento = $query->fetch(PDO::FETCH_ASSOC);
            if ($empresa_talento) {
                $this->empresa_talento_id = $empresa_talento['empresa_talento_id'];
                $this->talento_id = $empresa_talento['talento_id'];
                $this->empresa_id = $empresa_talento['empresa_id'];
            }
            return $this;
        } catch (PDOException $e) {
            error_log('USERMODEL::getId->PDOException ' . $e);
            return false;
        }
    }

    public function delete($empresa_talento_id)
    {
        try {
            $query = $this->prepare('DELETE FROM empresa_talento WHERE empresa_talento_id = :empresa_talento_id');
            $query->execute([
                'empresa_talento_id' => $empresa_talento_id
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::delete->PDOException ' . $e);
            return false;
        }
    }

    public function update()
    {
        try {
            $query = $this->prepare('UPDATE SET empresa_id = :empresa_id, talento_id = :talento_id WHERE empresa_talento_id = :empresa_talento_id');
            $query->execute([
                'empresa_talento_id' => $this->empresa_talento_id,
                'talento_id' => $this->talento_id,
                'empresa_id' => $this->empresa_id,
            ]);

            return true;
        } catch (PDOException $e) {
            error_log('USERMODEL::update->PDOException ' . $e);
            return false;
        }
    }

    // Pasar un arreglo para añadir un miembro
    public function from($array)
    {
        $this->empresa_talento_id = $array['empresa_id'];
        $this->talento_id = $array['talento_id'];
        $this->empresa_id = $array['empresa_id'];
    }

    public function exists($empresa_id)
    {
        try {
            $query = $this->prepare('SELECT empresa_id FROM empresa_talento WHERE empresa_id = :empresa_id');
            $query->execute(['empresa_id' => $empresa_id]);

            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            error_log('USERMODEL::exists->PDOException ' . $e);
            return false;
        }
    }

    public function getId()
    {
        return $this->empresa_talento_id;
    }

    public function setId($empresa_talento_id)
    {
        $this->empresa_talento_id = $empresa_talento_id;

        return $this;
    }

    public function getTalentoId()
    {
        return $this->talento_id;
    }

    public function setTalentoId($talento_id)
    {
        $this->talento_id = $talento_id;

        return $this;
    }

    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    public function setEmpresaId($empresa_id)
    {
        $this->empresa_id = $empresa_id;

        return $this;
    }

}
