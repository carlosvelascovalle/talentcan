<?php
/**
 * Controlador que también maneja las sesiones
 */
class SessionController extends Controller{
    
    private $userSession;
    private $username;
    private $userid;

    private $session;
    private $sites;

    private $user;
 
    function __construct(){
        parent::__construct();

        $this->init();
    }

    public function getUserSession(){
        return $this->userSession;
    }

    public function getUsername(){
        return $this->username;
    }

    public function getUserId(){
        return $this->userid;
    }

    /**
     * Inicializa el parser para leer el .json
     */
    private function init(){
        //se crea nueva sesión
        $this->session = new Session();
        //se carga el archivo json con la configuración de acceso
        $json = $this->getJSONFileConfig();
        // se asignan los sitios
        $this->sites = $json['sites'];
        // se asignan los sitios por default, los que cualquier rol tiene acceso
        $this->defaultSites = $json['default-sites'];

        // inicia el flujo de validación para determinar
        // el tipo de rol y permismos

        $this->validateSession();
    }
    /**
     * Abre el archivo JSON y regresa el resultado decodificado
     */
    private function getJSONFileConfig(){
        error_log('SESSIONCONTROLLER::getJSONFileConfig()');
        $string = file_get_contents("config/access.json");
        $json = json_decode($string, true);
        return $json;
    }

    /**
     * Implementa el flujo de autorización
     * para entrar a las páginas
     */
    function validateSession(){
        error_log('SESSIONCONTROLLER::validateSession()');
        //Si existe la sesión
        if($this->existsSession()){
            error_log('SESSIONCONTROLLER::validateSession() -> Existe Sesion');
            $rol = $this->getUserSessionData()->getRole();
            if($this->isPublic()){
                error_log( "SESSIONCONTROLLER::validateSession() => Sitio público, redirige al main de cada rol" . $rol );
                $this->redirectDefaultSiteByRole($rol);
            }else{
                if($this->isAuthorized($rol)){
                    error_log( "SESSIONCONTROLLER::validateSession() => autorizado, lo deja pasar" );
                    //si el usuario está en una página de acuerdo
                    // a sus permisos termina el flujo
                }else{
                    error_log( "SESSIONCONTROLLER::validateSession() => no autorizado, redirige al main de cada rol" );
                    // si el usuario no tiene permiso para estar en
                    // esa página lo redirije a la página de inicio
                    $this->redirectDefaultSiteByRole($rol);
                }
            }
        }else{
            error_log('SESSIONCONTROLLER::validateSession() -> NO existe sesión');
            //No existe ninguna sesión
            //se valida si el acceso es público o no
            if($this->isPublic()){
                error_log('SESSIONCONTROLLER::validateSession() public page');
                //la pagina es publica
                //no pasa nada
            }else{
                //la página no es pública
                //redirect al login
                error_log('SESSIONCONTROLLER::validateSession() redirect al login');
                header('location: '. constant('URL') . '');
            }
        }
    }
    /**
     * Valida si existe sesión, 
     * si es verdadero regresa el usuario actual
     */
    function existsSession(){
        if(!$this->session->exists()) return false;
        if($this->session->getCurrentUser() == NULL) return false;
        
        $userid = $this->session->getCurrentUser();
        
        error_log("SESSIONCONTROLLER::existsSession(): " . $userid);
        if($userid) return true;

        //return false;
    }

    function getUserSessionData(){
        error_log("SESSIONCONTROLLER::getUserSessionData()");
        $username = $this->session->getCurrentUser();
        $this->user = new UserModel();
        $this->user->getByUsername($username);
        error_log("sessionController::getUserSessionData(): " . $this->user->getId());
        error_log("sessionController::getUserSessionData(): " . $this->user->getUsername());
        return $this->user;
    }

    public function initialize($user){
        error_log("sessionController::initialize(): user: " . $user->getUsername());
        $this->session->setCurrentUser($user->getId());
        $this->authorizeAccess($user->getRole());
    }

    private function isPublic(){
        $currentURL = $this->getCurrentPage();
        error_log("sessionController::isPublic(): currentURL => " . $currentURL);
        $currentURL = preg_replace( "/\?.*/", "", $currentURL); //omitir get info
        for($i = 0; $i < sizeof($this->sites); $i++){
            if($currentURL === $this->sites[$i]['site'] && $this->sites[$i]['access'] === 'public'){
                return true;
            }
        }
        return false;
    }

    private function redirectDefaultSiteByRole($rol){
        $url = '';
        error_log("sessionController::redirectDefaultSiteByRole() => rol -> ".$rol);

        for($i = 0; $i < sizeof($this->sites); $i++){
            if($this->sites[$i]['rol'] === $rol){
               $url = '/' . $this->sites[$i]['site'];
            break;
            }
        }
        header('location: '.constant('URL').$url);
        
    }

    private function isAuthorized($rol){
        error_log("sessionController::isAuthorized() => rol -> ".$rol);
        $currentURL = $this->getCurrentPage();
        $currentURL = preg_replace( "/\?.*/", "", $currentURL); //omitir get info
        
        for($i = 0; $i < sizeof($this->sites); $i++){
            if($currentURL === $this->sites[$i]['site'] && $this->sites[$i]['rol'] === $rol){
                return true;
            }
        }
        return false;
    }

    private function getCurrentPage(){
        
        $actual_link = trim("$_SERVER[REQUEST_URI]");
        $url = explode('/', $actual_link);
        error_log("sessionController::getCurrentPage(): actualLink =>" . $actual_link . ", url => " . $url[1]);
        return $url[1];
    }

    function authorizeAccess($rol){
        error_log("sessionController::authorizeAccess(): rol: $rol");
        $this->redirect($this->defaultSites[$rol]);
        /*switch($rol){
            case 'empresa':
                $this->redirect($this->defaultSites[$rol]);
            break;
            case 'admin':
                $this->redirect($this->defaultSites[$rol]);
            break;
            default:*/
    }

    function logout(){
        $this->session->closeSession();
    }
}
