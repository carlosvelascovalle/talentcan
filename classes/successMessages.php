<?php

class SuccessMessages{

    // MD5
    //ERROR_CONTROLLER_METHOD_ACTION
    const SUCCESS_SIGNUP_NEWUSER = "c74163200ec1e78de1223179774c0318";

    private $successList = [];

    public function __construct()
    {
        $this->successList = [
            SuccessMessages::SUCCESS_SIGNUP_NEWUSER => 'Nuevo usuario registrado correctamente'
        ];
    }

    public function get($hash)
    {
        return $this->successList[$hash];
    }

    public function existKey($key)
    {
        if (array_key_exists($key, $this->successList)) {
            return true;
        } else {
            return false;
        }
    }
}