<?php

class Session
{
    private $sessionName = 'user';

    
    function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
 
    }

    public function setCurrentUser($user)
    {
        error_log('Session::setCurrentUser() -> ' . $user);
        $_SESSION[$this->sessionName] = $user;
    }

    public function getCurrentUser()
    {
        error_log('Session::getCurrentUser() -> ' . $_SESSION[$this->sessionName]);
        return $_SESSION[$this->sessionName];
    }

    public function closeSession()
    {
        error_log('Session::closeSession()');
        session_unset();
        session_destroy();
        header('Location: login.php');
    }

    public function exists()
    {
        error_log('Session::exists()');
        return isset($_SESSION[$this->sessionName]);
    }
}
